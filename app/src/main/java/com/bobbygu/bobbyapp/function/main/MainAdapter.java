package com.bobbygu.bobbyapp.function.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;

import com.bobbygu.bobbyapp.function.main.discover.DiscoverFragment;
import com.bobbygu.bobbyapp.function.main.home.HomeFragmemt;
import com.bobbygu.bobbyapp.function.main.mine.MineFragment;
import com.bobbygu.bobbyapp.function.main.product.ProductFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * MainAdapter
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/26 8:47 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class MainAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments = new ArrayList<>();

    public MainAdapter(FragmentActivity activity) {
        super(activity.getSupportFragmentManager());
        fragments.add(HomeFragmemt.Companion.newInstance());
        fragments.add(ProductFragment.Companion.newInstance());
        fragments.add(DiscoverFragment.Companion.newInstance());
        fragments.add(MineFragment.Companion.newInstance());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
