package com.bobbygu.bobbyapp.function.main.product

import com.bobbygu.lib.mvvm.common.BaseView
import com.bobbygu.bobbyapp.base.MyBaseM
import com.bobbygu.bobbyapp.base.MyBaseVM

/**
 *
 * # time:   2017/10/26 15:51
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
interface ProductContract {
    interface View : BaseView

    abstract class ViewModel : MyBaseVM<View, Model>() {
        abstract fun loadData()
    }

    abstract class Model : MyBaseM<ViewModel>() {
        abstract fun loadData()
    }
}