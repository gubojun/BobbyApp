package com.bobbygu.bobbyapp.function.main;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * MainViewModel
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/19 11:06 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class MainViewModel extends MainContract.ViewModel {
    public final ObservableField<String> username = new ObservableField<>();
    public final ObservableField<String> nickname = new ObservableField<>();
    public final ObservableField<Integer> age = new ObservableField<>();
    private ArrayList<MainViewModel> userList = new ObservableArrayList<>();

    @Override
    protected void init() {
    }

    @Override
    public void loadData() {
        init();
        getModel().loadData();
    }

    @Override
    public ArrayList<MainViewModel> getUserList() {
        return userList;
    }

    @Override
    public void returnUserList(List<MainViewModel> list) {
        Log.d("MainViewModel", list.toString());
//        username.set(list.get(0).getUser());
//        nickname.set(list.get(0).getPassword());
        userList.clear();
        userList.addAll(list);
    }
}
