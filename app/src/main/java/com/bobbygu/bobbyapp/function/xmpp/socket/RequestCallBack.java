package com.bobbygu.bobbyapp.function.xmpp.socket;

import com.bobbygu.bobbyapp.function.xmpp.protocol.BasicProtocol;

/**
 * RequestCallBack
 * <p>
 * author: 顾博君 <br>
 * time:   2018/2/1 14:45 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public interface RequestCallBack {
    void onSuccess(BasicProtocol msg);

    void onFailed(int errorCode, String msg);
}