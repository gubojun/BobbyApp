package com.bobbygu.bobbyapp.function.login;

import android.databinding.ObservableField;
import android.widget.Toast;

import com.bobbygu.bobbyapp.base.MyApplication;

/**
 * LoginViewModel
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/23 15:50 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class LoginViewModel extends LoginContract.ViewModel {
    public final ObservableField<String> username = new ObservableField<>();
    public final ObservableField<String> password = new ObservableField<>();

    @Override
    protected void init() {
    }

    @Override
    public void doLogin() {
        getModel().doLogin(username.get(), password.get());
    }

    @Override
    public void doRegist() {
        getModel().doRegist(username.get(), password.get());
    }

    @Override
    public void loginSuccess() {
        Toast.makeText(MyApplication.getInstance(), "登录成功", Toast.LENGTH_SHORT).show();
        getView().finish();
    }
}
