package com.bobbygu.bobbyapp.function

import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.LayoutHelper
import com.alibaba.android.vlayout.VirtualLayoutManager
import com.alibaba.android.vlayout.layout.*
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel
import com.bobbygu.bobbyapp.R
import java.util.*

@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
class VLayoutActivity<VM : BaseViewModel<*, *>, M : BaseModel<*>> : BaseActivity<VM, M>() {
    override fun getLayoutId() = R.layout.activity_vlayout

    override fun initView() {
        val recyclerView = findViewById<RecyclerView>(R.id.rv_container)
        //----------------------------------------------------------
        //绑定VirtualLayoutManager
        val layoutManager = VirtualLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        //设置Item之间的间隔
        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
                outRect.set(5, 5, 5, 5)
            }
        })
        //----------------------------------------------------------
        //设置线性布局
        val linearLayoutHelper = LinearLayoutHelper()
        //设置Item个数
        linearLayoutHelper.itemCount = 5
        //设置间隔高度
        linearLayoutHelper.setDividerHeight(5)
        //设置布局底部与下个布局的间隔
        linearLayoutHelper.marginBottom = 100
        //设置Adapter列表
        val adapters = LinkedList<DelegateAdapter.Adapter<*>>()
        adapters.add(object : MyAdapter(this, linearLayoutHelper, 5) {
            override fun onBindViewHolder(holder: MyAdapter.MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                if (position == 0) {
                    holder.tv1.text = "linearLayout"
                }
            }
        })
        //----------------------------------------------------------
        //设置Grid布局
        val gridLayoutHelper = GridLayoutHelper(4)
        //是否自动扩展
        gridLayoutHelper.setAutoExpand(false)
        gridLayoutHelper.setGap(5)
        //自定义设置某些位置的Item的占格数
        gridLayoutHelper.setSpanSizeLookup(object : GridLayoutHelper.SpanSizeLookup() {
            override fun getSpanSize(position: Int) = if (position > 15) 2 else 1
        })
        gridLayoutHelper.marginBottom = 100
        adapters.add(object : MyAdapter(this, gridLayoutHelper, 23) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                if (position == 0) {
                    holder.tv1.setText("gridLayout")
                }
            }
        })
        //----------------------------------------------------------
        //设置固定布局
        val fixLayoutHelper = FixLayoutHelper(FixLayoutHelper.TOP_LEFT, 0, 0)
        adapters.add(object : MyAdapter(this, fixLayoutHelper, 1) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                val layoutParams = ViewGroup.LayoutParams(300, 80)
                holder.tv1.setText("fixLayout")
                holder.itemView.layoutParams = layoutParams
                holder.itemView.setBackgroundColor(Color.BLUE)
            }
        })
        //----------------------------------------------------------
        //设置滚动固定布局
        val scrollFixLayoutHelper = ScrollFixLayoutHelper(ScrollFixLayoutHelper.TOP_RIGHT, 0, 0)
        scrollFixLayoutHelper.showType = ScrollFixLayoutHelper.SHOW_ON_LEAVE
        adapters.add(object : MyAdapter(this, scrollFixLayoutHelper, 1) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                val layoutParams = ViewGroup.LayoutParams(300, 90)
                holder.tv1.setText("scrollFixLayout")
                holder.itemView.layoutParams = layoutParams
                holder.itemView.setBackgroundColor(Color.BLUE)
            }
        })
        //----------------------------------------------------------
        //设置浮动布局
        val floatLayoutHelper = FloatLayoutHelper()
        //设置初始位置
        floatLayoutHelper.setDefaultLocation(20, 250)
        adapters.add(object : MyAdapter(this, floatLayoutHelper, 1) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                val layoutParams = ViewGroup.LayoutParams(300, 80)
                holder.tv1.setText("floatLayout")
                holder.itemView.layoutParams = layoutParams
                holder.itemView.setBackgroundColor(Color.RED)
            }
        })
        //----------------------------------------------------------
        //设置栏格布局
        val columnLayoutHelper = ColumnLayoutHelper()
        columnLayoutHelper.itemCount = 5
        columnLayoutHelper.zIndex = 10
        columnLayoutHelper.setWeights(floatArrayOf(30f, 10f, 30f, 20f, 10f))
        columnLayoutHelper.marginBottom = 100
        adapters.add(object : MyAdapter(this, columnLayoutHelper, 5) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                if (position == 0) {
                    holder.tv1.setText("columnLayout")
                }
            }
        })
        //----------------------------------------------------------
        //设置通栏布局
        val singleLayoutHelper = SingleLayoutHelper()
        singleLayoutHelper.marginBottom = 100
        adapters.add(object : MyAdapter(this, singleLayoutHelper, 1) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                holder.tv1.setText("singleLayout")
            }
        })
        //----------------------------------------------------------
        //设置一拖N布局
        val onePlusNLayoutHelper = OnePlusNLayoutHelper(5)
        onePlusNLayoutHelper.marginBottom = 100
        adapters.add(object : MyAdapter(this, onePlusNLayoutHelper, 5) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                val layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150)
                holder.itemView.layoutParams = layoutParams
                if (position == 0) {
                    holder.tv1.setText("onePlusNLayout")
                }
            }
        })
        //----------------------------------------------------------
        //设置Sticky布局
        val stickyLayoutHelper = StickyLayoutHelper()
        stickyLayoutHelper.setStickyStart(true)
        adapters.add(object : MyAdapter(this, stickyLayoutHelper, 1) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                holder.tv1.setText("stickyLayout")
            }
        })
        //----------------------------------------------------------
        //设置瀑布流布局
        val staggeredGridLayoutHelper = StaggeredGridLayoutHelper()
        staggeredGridLayoutHelper.lane = 3
        staggeredGridLayoutHelper.hGap = 5
        staggeredGridLayoutHelper.vGap = 5
        staggeredGridLayoutHelper.marginBottom = 100
        adapters.add(object : MyAdapter(this, staggeredGridLayoutHelper, 31) {
            override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
                super.onBindViewHolder(holder, position)
                val layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, 150 + position % 5 * 20)
                holder.itemView.layoutParams = layoutParams
                if (position == 0) {
                    holder.tv1.text = "staggeredGridLayout"
                }
            }
        })

        //绑定delegateAdapter
        val delegateAdapter = DelegateAdapter(layoutManager)
        delegateAdapter.setAdapters(adapters)
        recyclerView.adapter = delegateAdapter
    }

    open class MyAdapter @JvmOverloads constructor(
            private val context: Context,
            private val layoutHelper: LayoutHelper,
            count: Int,
            private val layoutParams: RecyclerView.LayoutParams
            = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 300)
    ) : DelegateAdapter.Adapter<MyAdapter.MainViewHolder>() {
        private var count = 0

        init {
            this.count = count
        }

        override fun onCreateLayoutHelper(): LayoutHelper = layoutHelper

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder =
                MainViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item, parent, false))

        override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
            holder.tv1.text = "$position"
            if (position > 7) {
                holder.itemView.setBackgroundColor(0x66cc0000 + (position - 6) * 128)
            } else if (position % 2 == 0) {
                holder.itemView.setBackgroundColor(-0x55dd00de)
            } else {
                holder.itemView.setBackgroundColor(-0x3300dd01)
            }
        }

        override fun getItemCount() = count

        inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv1: TextView = itemView.findViewById(R.id.item_tv1)
        }
    }

    companion object {
        fun <VM : BaseViewModel<*, *>, M : BaseModel<*>> startActivity(activity: BaseActivity<VM, M>) {
            activity.startActivity(VLayoutActivity::class.java)
        }
    }
}
