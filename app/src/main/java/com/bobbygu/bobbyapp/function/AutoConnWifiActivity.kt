package com.bobbygu.bobbyapp.function

import android.annotation.SuppressLint
import android.view.View
import com.bobbygu.bobbyapp.R
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel
import com.bobbygu.bobbyapp.utils.WifiOperator
import kotlinx.android.synthetic.main.activity_auto_conn_wifi.*
import android.os.Handler
import android.os.Message
import android.text.method.ScrollingMovementMethod


/**
 * Created by BobbyGu on 2018/1/25.
 */
@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
class AutoConnWifiActivity<VM : BaseViewModel<*, *>, M : BaseModel<*>> : BaseActivity<VM, M>() {
    var wifiOperator: WifiOperator? = null
    override fun getLayoutId() = R.layout.activity_auto_conn_wifi
    @SuppressLint("WifiManagerLeak")
    override fun initView() {
        WifiOperator.setContext(this)
        WifiOperator.setHandler(@SuppressLint("HandlerLeak") object : Handler() {
            override fun handleMessage(msg: Message) {
                // 操作界面
                tv_msg.text = tv_msg.text.toString() + "\n" + msg.obj + ""
            }
        })
        wifiOperator = WifiOperator.getInstance()
        tv_msg.movementMethod = ScrollingMovementMethod.getInstance()
    }

    fun openWifi(view: View) {
        wifiOperator?.openWifi()
    }

    fun closeWifi(view: View) {
        wifiOperator?.closeWifi()
    }

    fun connectWifi(view: View) {
        val name = et_input.text.toString()
        val pass = et_pass.text.toString()
        wifiOperator?.addNetWorkAndConnect(name, pass, WifiOperator.WifiCipherType.WPA2)
    }

    fun autoConn(view: View) {
        val name = et_input.text.toString()
        val pass = et_pass.text.toString()
        wifiOperator?.autoConnect(name, pass, WifiOperator.WifiCipherType.WPA2)
    }

    fun autoConnTestNet(view: View) {
        val name = "MobileBankTestZX"
        val pass = "wxcsiitest1"
        wifiOperator?.autoConnect(name, pass, WifiOperator.WifiCipherType.WPA2)
    }

    companion object {

        /**
         * @param activity BaseActivity
         */
        fun startActivity(activity: BaseActivity<*, *>) {
            activity.startActivity(AutoConnWifiActivity::class.java)
        }
    }
}