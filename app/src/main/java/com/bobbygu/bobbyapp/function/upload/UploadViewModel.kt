package com.bobbygu.bobbyapp.function.upload

import android.graphics.BitmapFactory
import android.widget.Toast
import com.bobbygu.bobbyapp.utils.FileUtils


/**
 *
 * # time:   2017/10/24 8:55
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since
 */

class UploadViewModel : UploadContract.ViewModel() {
    override fun init() {
    }

    override fun uploadFile() {
        val `is` = context.assets.open("IMG_1155.JPG")
//        val bitmap = BitmapFactory.decodeStream(`is`)
//        view.returnBitmap(bitmap)
//        model.uploadFile(bitmap)
        val bytes = FileUtils.getBytes(`is`)
        if (bytes.isEmpty()) {
            Toast.makeText(context, "bytes is null", Toast.LENGTH_LONG).show()
            return
        }
        val filePath = FileUtils.writeData(bytes, "temp.jpg")
        if (filePath != null)
            model.uploadFile(filePath)
        else
            Toast.makeText(context, "filePath is null", Toast.LENGTH_LONG).show()
    }

    override fun returnImgPath(imgPath: String?) {
        val bitmap = BitmapFactory.decodeFile(imgPath)
        view.returnBitmap(bitmap)
    }

    fun getImg() {
        model.getImg()
    }
}