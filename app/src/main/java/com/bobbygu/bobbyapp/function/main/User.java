package com.bobbygu.bobbyapp.function.main;

/**
 * Created by 顾博君 on 2017/10/19.
 */

public class User {
    private String name;
    private String password;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
