package com.bobbygu.bobbyapp.function.main;

import com.bobbygu.lib.mvvm.common.BaseView;
import com.bobbygu.bobbyapp.base.MyBaseM;
import com.bobbygu.bobbyapp.base.MyBaseVM;

import java.util.ArrayList;
import java.util.List;

/**
 * MainContract
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/19 11:01 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public interface MainContract {
    interface View extends BaseView {
    }

    abstract class ViewModel extends MyBaseVM<View, Model> {
        public abstract void loadData();

        public abstract void returnUserList(List<MainViewModel> list);

        public abstract ArrayList<MainViewModel> getUserList();
    }

    abstract class Model extends MyBaseM<ViewModel> {
        public abstract void loadData();
    }
}
