package com.bobbygu.bobbyapp.function

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bobbygu.bobbyapp.R

/**
 *
 * # time:   2017/11/1 18:22
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since
 */
class SimpleCardFragment : Fragment() {
    private var mTitle: String? = null

    companion object {
        fun getInstance(title: String): SimpleCardFragment {
            val sf = SimpleCardFragment()
            sf.mTitle = title
            return sf
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fr_simple_card, null)
        val card_title_tv = v.findViewById<TextView>(R.id.card_title_tv)
        card_title_tv.text = mTitle
        return v
    }
}