package com.bobbygu.bobbyapp.function.replugin

import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.os.Handler
import android.widget.Toast
import com.bobbygu.libnet.utils.ACache
import com.bobbygu.bobbyapp.bean.PluginBean
import com.orhanobut.logger.Logger


/**
 * 插件化
 * # time: 2017/11/29 13:45
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class RepluginViewModel : RepluginContract.ViewModel() {
    private val pluginList = ObservableArrayList<PluginBean>()

    override fun init() {
        Handler().postDelayed({ model.getPluginMsg() }, 500)
    }

    override fun getPluginList(): ObservableList<PluginBean> = pluginList

    override fun returnPluginList(pluginList: List<PluginBean>) {
        Logger.d("returnPluginList:" + pluginList)
        this.pluginList.clear()
        this.pluginList.addAll(pluginList)
    }


    override fun downloadPlugin(url: String) {
        model.downloadPlugin(url)
    }

    override fun returnDownloadProgress(progress: Int) {
        view.returnDownloadProgress(progress)
    }

    override fun returnFilePath(fileName: String, filePath: String?) {
        ACache.get(context).put(fileName, filePath)
    }

    fun onItemClick(bean: PluginBean) {
        Logger.d("onItemClick:" + bean.name)
        Toast.makeText(context, "click:" + bean.name, Toast.LENGTH_LONG).show()
    }

    fun onItemLongClick(bean: PluginBean): Boolean {
        Toast.makeText(context, "longClick:" + bean.name, Toast.LENGTH_LONG).show()
        return true;
    }
}