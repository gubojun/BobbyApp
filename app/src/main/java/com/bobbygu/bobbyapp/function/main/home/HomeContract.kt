package com.bobbygu.bobbyapp.function.main.home

import com.bobbygu.lib.mvvm.common.BaseView
import com.bobbygu.bobbyapp.base.MyBaseM
import com.bobbygu.bobbyapp.base.MyBaseVM
import com.bobbygu.bobbyapp.bean.AdPicBean

/**
 *
 * # time:   2017/11/7 11:39
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class HomeContract {
    interface View : BaseView {
        fun initAD(list: ArrayList<String>)
    }

    abstract class ViewModel : MyBaseVM<View, Model>() {
        abstract fun returnAdList(adPicList: List<AdPicBean>)
        abstract fun getAdList()
    }

    abstract class Model : MyBaseM<ViewModel>() {
        abstract fun getAdList()
    }
}