package com.bobbygu.bobbyapp.function.aidlSample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.bobbygu.bobbyapp.IMyAidlInterface;

import java.util.ArrayList;
import java.util.List;


/**
 * AIDLService aidl服务代码，客户端代码在bobbytest工程中
 * <p>
 * author: 顾博君 <br>
 * time:   2017/12/5 18:11 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class AIDLService extends Service {
    MyBinder myBinder;
    @Override
    public void onCreate() {
        super.onCreate();
        myBinder = new MyBinder();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public class MyBinder extends IMyAidlInterface.Stub {

        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {
        }

        @Override
        public String getColor() throws RemoteException {
            return "red";
        }

        @Override
        public double getWeight() throws RemoteException {
            return 333.3d;
        }

        @Override
        public Book getBook() throws RemoteException {
            Book book = new Book();
            book.setName("Bobby's book");
            book.setPrice(100);
            return book;
        }
    }
}
