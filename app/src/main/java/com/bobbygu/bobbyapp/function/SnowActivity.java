package com.bobbygu.bobbyapp.function;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;

import com.bobbygu.bobbyapp.R;
import com.bobbygu.bobbyapp.view.showview.SimpleSnowView;
import com.bobbygu.bobbyapp.view.showview.SnowView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * @author BobbyGu
 * @date 2017/12/28
 */

public class SnowActivity extends Activity {
    FrameLayout fl;
    SnowView sv;
    SimpleSnowView ssv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snow);
        fl = findViewById(R.id.fl);
        sv = findViewById(R.id.snow1);
        ssv = findViewById(R.id.snow2);
        fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sv.getVisibility() == View.VISIBLE) {
                    sv.setVisibility(View.GONE);
                    ssv.setVisibility(View.VISIBLE);
                    new SweetAlertDialog(SnowActivity.this)
                            .setTitleText(getString(R.string.dialog_title))
                            .setContentText("简单的雪")
                            .show();
                } else {
                    sv.setVisibility(View.VISIBLE);
                    ssv.setVisibility(View.GONE);
                    new SweetAlertDialog(SnowActivity.this)
                            .setTitleText(getString(R.string.dialog_title))
                            .setContentText("认真的雪")
                            .show();
                }
            }
        });
    }

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, SnowActivity.class);
        activity.startActivity(intent);
    }
}
