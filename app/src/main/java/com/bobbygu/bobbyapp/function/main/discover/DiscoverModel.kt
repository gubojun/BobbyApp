package com.bobbygu.bobbyapp.function.main.discover

import android.widget.Toast
import com.alibaba.fastjson.JSON
import com.bobbygu.bobbyapp.base.MyApplication
import com.bobbygu.bobbyapp.bean.UserBean
import com.bobbygu.bobbyapp.network.HttpAction
import java.util.*

/**
 *
 * # time:   2017/10/26 9:35
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class DiscoverModel : DiscoverContract.Model() {
    override fun loadData() {
        val map = HashMap<String, String>()
        map.put("BankId", "23423")
        map.put("LoginType", "P")
        post(HttpAction.ShowUser, map, { jsonObject ->
            val list = JSON.parseArray(jsonObject.getJSONArray("data").toJSONString(), UserBean::class.java)
            viewModel.returnUserList(list)
        }, { errMsg, _ ->
            Toast.makeText(MyApplication.getInstance(), errMsg, Toast.LENGTH_SHORT).show()
        })
    }
}