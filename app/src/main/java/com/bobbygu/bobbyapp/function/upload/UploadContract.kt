package com.bobbygu.bobbyapp.function.upload

import android.graphics.Bitmap
import com.bobbygu.lib.mvvm.common.BaseView
import com.bobbygu.bobbyapp.base.MyBaseM
import com.bobbygu.bobbyapp.base.MyBaseVM

/**
 *
 * # time:   2017/10/23 16:35
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since
 */
interface UploadContract {
    interface View : BaseView {
        fun returnBitmap(bitmap: Bitmap)
    }

    abstract class ViewModel : MyBaseVM<View, Model>() {
        abstract fun uploadFile()
        abstract fun returnImgPath(imgPath: String?)
    }

    abstract class Model : MyBaseM<ViewModel>() {
        abstract fun uploadFile(bitmap: Bitmap)
        abstract fun uploadFile(filePath: String?)
        abstract fun getImg()
    }
}