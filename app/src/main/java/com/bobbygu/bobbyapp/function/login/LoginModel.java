package com.bobbygu.bobbyapp.function.login;

import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.bobbygu.bobbyapp.base.MyApplication;
import com.bobbygu.bobbyapp.network.HttpAction;

import java.util.HashMap;
import java.util.Map;

/**
 * LoginModel
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/23 9:20 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class LoginModel extends LoginContract.Model {
    @Override
    public void doLogin(String username, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("Name", username);
        map.put("Pass", password);
        post(HttpAction.Companion.getLogin(), map, new SuccessCallBack() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                Log.d("LoginModel", "json:" + jsonObject.toJSONString());
//                        List<MainModel> list = JSON.parseArray(jsonObject.getJSONArray("data").toJSONString(), MainModel.class);
                getViewModel().loginSuccess();
            }
        }, new ErrorCallBack() {
            @Override
            public void onError(String errMsg, String errCode) {
                Toast.makeText(MyApplication.getInstance(), errMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void doRegist(String username, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("Name", username);
        map.put("Pass", password);

        post(HttpAction.Companion.getRegister(), map, new SuccessCallBack() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                Log.d("LoginModel", "json:" + jsonObject.toJSONString());
                Toast.makeText(MyApplication.getInstance(), "注册成功", Toast.LENGTH_SHORT).show();
            }
        }, new ErrorCallBack() {
            @Override
            public void onError(String errMsg, String errCode) {
                Toast.makeText(MyApplication.getInstance(), errMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
