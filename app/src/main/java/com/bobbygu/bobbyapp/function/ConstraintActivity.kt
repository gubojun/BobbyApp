package com.bobbygu.bobbyapp.function

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.utils.FontHelper
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel

class ConstraintActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constraint)
        FontHelper.injectFont(findViewById(R.id.linearLayout))
    }

    companion object {
        fun <VM : BaseViewModel<*, *>, M : BaseModel<*>> startActivity(activity: BaseActivity<VM, M>) {
            activity.startActivity(ConstraintActivity::class.java)
        }
    }
}
