package com.bobbygu.bobbyapp.function.main.discover

import com.bobbygu.lib.mvvm.adapter.BindingRecyclerViewAdapters
import com.bobbygu.lib.mvvm.adapter.ItemBinding
import com.bobbygu.lib.mvvm.adapter.LayoutManagers
import com.bobbygu.bobbyapp.BR
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.base.BaseLazyFragment
import com.bobbygu.bobbyapp.databinding.FragmentDiscoverBinding

/**
 * 发现页
 * # time:   2017/10/26 9:07
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class DiscoverFragment : BaseLazyFragment<DiscoverViewModel, DiscoverModel>(), DiscoverContract.View {
    override fun onUserVisible() {
//        Handler().postDelayed({
        viewModel.loadData()//获取数据
//        }, 2000)
    }

    override fun onFirstUserVisible() {
        viewModel.loadData()//获取数据
    }

    override fun getLayoutId(): Int = R.layout.fragment_discover
    override fun getBR(): Int = BR.vm
    override fun initView() {
        val recyclerView = (viewDataBinding as FragmentDiscoverBinding).rvContainer
        BindingRecyclerViewAdapters.setAdapter(
                recyclerView,
                ItemBinding.of(BR.user, R.layout.item_user),
                viewModel.getUserList(), null, null, null)
        recyclerView.setHasFixedSize(true)//非瀑流RecyclerView减少资源浪费
        BindingRecyclerViewAdapters.setLayoutManager(recyclerView, LayoutManagers.linear())
    }

    companion object {
        fun newInstance() = DiscoverFragment()
    }
}