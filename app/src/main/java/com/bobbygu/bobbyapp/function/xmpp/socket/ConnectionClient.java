package com.bobbygu.bobbyapp.function.xmpp.socket;

import com.bobbygu.bobbyapp.function.xmpp.protocol.DataProtocol;

/**
 * ConnectionClient
 * <p>
 * author: 顾博君 <br>
 * time:   2018/2/1 14:53 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class ConnectionClient {
    private boolean isClosed;

    private ClientRequestTask mClientRequestTask;

    public ConnectionClient(RequestCallBack requestCallBack) {
        mClientRequestTask = new ClientRequestTask(requestCallBack);
        new Thread(mClientRequestTask).start();
    }

    public void addNewRequest(DataProtocol data) {
        if (mClientRequestTask != null && !isClosed) {
            mClientRequestTask.addRequest(data);
        }
    }

    public void closeConnect() {
        isClosed = true;
        mClientRequestTask.stop();
    }
}
