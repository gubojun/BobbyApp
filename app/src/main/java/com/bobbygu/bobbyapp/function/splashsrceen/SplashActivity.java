package com.bobbygu.bobbyapp.function.splashsrceen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bobbygu.bobbyapp.function.main.MainActivity;
import com.bobbygu.lib.mvvm.common.BaseActivity;
import com.bobbygu.bobbyapp.R;
import com.bobbygu.libspeech.TtsDemo;

/**
 * SplashActivity
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/25 10:15 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class SplashActivity extends BaseActivity {
    private View mContentView;

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initView() {
        mContentView = findViewById(R.id.fullscreen_content);
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setSlideBack(false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mContentView.postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity.startActivity(SplashActivity.this);
//                Intent intent = new Intent(SplashActivity.this, TtsDemo.class);
//                startActivity(intent);

                SplashActivity.this.finishActivity();
                overridePendingTransition(-1, android.R.anim.slide_out_right);
            }
        }, 3000);
    }
}
