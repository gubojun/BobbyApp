package com.bobbygu.bobbyapp.function.replugin

import android.databinding.ObservableList
import com.bobbygu.lib.mvvm.common.BaseView
import com.bobbygu.bobbyapp.base.MyBaseM
import com.bobbygu.bobbyapp.base.MyBaseVM
import com.bobbygu.bobbyapp.bean.PluginBean

/**
 * 360插件化使用
 * # time: 2017/11/29 13:39
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
interface RepluginContract {
    interface View : BaseView {
        fun returnDownloadProgress(progress: Int)
    }

    abstract class ViewModel : MyBaseVM<View, Model>() {
        abstract fun getPluginList(): ObservableList<PluginBean>
        /**
         * 返回插件列表
         */
        abstract fun returnPluginList(pluginList: List<PluginBean>)

        /**
         * 下载插件
         * @param url 下载地址
         */
        abstract fun downloadPlugin(url: String)

        abstract fun returnDownloadProgress(progress: Int)
        abstract fun returnFilePath(fileName: String, filePath: String?)
    }

    abstract class Model : MyBaseM<ViewModel>() {
        abstract fun getPluginMsg()
        abstract fun downloadPlugin(url: String)
    }
}