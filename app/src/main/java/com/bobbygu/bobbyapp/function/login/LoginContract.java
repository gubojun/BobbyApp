package com.bobbygu.bobbyapp.function.login;

import com.bobbygu.lib.mvvm.common.BaseView;
import com.bobbygu.bobbyapp.base.MyBaseM;
import com.bobbygu.bobbyapp.base.MyBaseVM;

/**
 * LoginContract 登录契约类
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/23 9:18 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

interface LoginContract {
    interface View extends BaseView {
    }

    abstract class ViewModel extends MyBaseVM<View, Model> {

        public abstract void doLogin();

        public abstract void doRegist();

        public abstract void loginSuccess();
    }

    abstract class Model extends MyBaseM<ViewModel> {
        public abstract void doLogin(String username, String password);

        public abstract void doRegist(String username, String password);
    }
}
