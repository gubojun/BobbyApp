package com.bobbygu.bobbyapp.function.main.product

import android.graphics.drawable.AnimationDrawable
import android.widget.LinearLayout
import com.bobbygu.bobbyapp.BR
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.base.BaseLazyFragment
import com.facebook.drawee.backends.pipeline.Fresco
import android.graphics.drawable.Animatable
import android.net.Uri
import com.bobbygu.libnet.common.ApiConstants
import com.bobbygu.libnet.common.HostType
import com.facebook.imagepipeline.animated.base.AbstractAnimatedDrawable
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.orhanobut.logger.Logger


/**
 * 产品页
 * # time:   2017/10/26 15:44
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class ProductFragment : BaseLazyFragment<ProductViewModel, ProductModel>(), ProductContract.View {
    private var anim: AnimationDrawable? = null
    var animatable: AbstractAnimatedDrawable? = null
    override fun getLayoutId(): Int = R.layout.fragment_product
    override fun getBR(): Int = BR.vm

    override fun onResume() {
        super.onResume()
        if (anim != null && !anim!!.isRunning)
            anim!!.start()
    }

    override fun onPause() {
        super.onPause()
        if (anim != null && anim!!.isRunning)
            anim!!.stop()
    }

    override fun initView() {
        val container = viewDataBinding.root.findViewById<LinearLayout>(R.id.ll_container)
        anim = container.background as AnimationDrawable
        anim?.setEnterFadeDuration(4000)
        anim?.setExitFadeDuration(2000)
        //----------------------------------------------------------
        //获取webp图片框，使用kotlin extensions 方式获取控件会报错
        val sdv = viewDataBinding.root.findViewById<SimpleDraweeView>(R.id.sdv)
        val sdv2 = viewDataBinding.root.findViewById<SimpleDraweeView>(R.id.sdv2)
        //获取动画对象
        val controllerListener = object : BaseControllerListener<ImageInfo>() {
            override fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, anim: Animatable?) {
                anim?.start()
                animatable = anim as AbstractAnimatedDrawable?
            }
        }
        Logger.d(ApiConstants.getHost(HostType.PRODUCT_HOST) + "imageDir/9UCkIvlp.webp")

        val controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(ApiConstants.getHost(HostType.PRODUCT_HOST) + "imageDir/9UCkIvlp.webp"))
                .setAutoPlayAnimations(true)
                .setControllerListener(controllerListener)
                .build()

        sdv.controller = controller
        //控制动画
        sdv.setOnClickListener {
            if (animatable != null) {
                if (animatable!!.isRunning) {
                    animatable!!.pause()
                } else {
                    animatable!!.start()
                }
            }
        }
        //这种方式也可以
//        sdv.setOnClickListener {
//            val animatable = sdv?.controller?.animatable
//            if (animatable != null) {
//                if (animatable.isRunning) {
//                    animatable.stop()
//                } else {
//                    animatable.start()
//                }
//            }
//        }

        //----------------------------------------------------------
        sdv2.controller = Fresco.newDraweeControllerBuilder()
//                .setUri(Uri.parse("asset:///test.webp"))
                .setUri(Uri.parse("res:///" + R.drawable.test))
                .setAutoPlayAnimations(true)
                .build()
    }

    companion object {
        fun newInstance() = ProductFragment()
    }
}