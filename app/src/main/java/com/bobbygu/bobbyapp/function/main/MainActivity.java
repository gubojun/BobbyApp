package com.bobbygu.bobbyapp.function.main;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import com.bobbygu.bobbyapp.function.AutoConnWifiActivity;
import com.bobbygu.bobbyapp.function.ConstraintActivity;
import com.bobbygu.bobbyapp.function.SnowActivity;
import com.bobbygu.bobbyapp.function.TTSActivity;
import com.bobbygu.bobbyapp.function.WebActivity;
import com.bobbygu.bobbyapp.function.xmpp.BackService;
import com.bobbygu.bobbyapp.function.xmpp.protocol.BasicProtocol;
import com.bobbygu.bobbyapp.function.xmpp.protocol.DataAckProtocol;
import com.bobbygu.bobbyapp.function.xmpp.protocol.DataProtocol;
import com.bobbygu.bobbyapp.function.xmpp.protocol.PingAckProtocol;
import com.bobbygu.bobbyapp.function.xmpp.socket.ConnectionClient;
import com.bobbygu.bobbyapp.function.xmpp.socket.RequestCallBack;
import com.bobbygu.bobbyapp.utils.MailUtils;
import com.bobbygu.lib.mvvm.common.BaseActivity;
import com.bobbygu.lib.mvvm.common.BaseModel;
import com.bobbygu.lib.mvvm.common.BaseViewModel;
import com.bobbygu.libnet.utils.ACache;
import com.bobbygu.bobbyapp.BR;
import com.bobbygu.bobbyapp.R;
import com.bobbygu.bobbyapp.databinding.ActivityMainBinding;
import com.bobbygu.bobbyapp.function.contentprovider.ProviderActivity;
import com.bobbygu.bobbyapp.function.FlycoTabLayoutActivity;
import com.bobbygu.bobbyapp.function.SegmentTabActivity;
import com.bobbygu.bobbyapp.function.VLayoutActivity;
import com.bobbygu.bobbyapp.function.dragphoto.DragPhotoMainActivity;
import com.bobbygu.bobbyapp.function.login.LoginActivity;
import com.bobbygu.bobbyapp.function.replugin.RepluginActivity;
import com.bobbygu.bobbyapp.function.update.UpdateActivity;
import com.bobbygu.bobbyapp.function.upload.UploadActivity;
import com.bobbygu.libnet.utils.DeviceUtil;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.orhanobut.logger.Logger;

import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import tourguide.tourguide.ChainTourGuide;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Sequence;
import tourguide.tourguide.ToolTip;

public class MainActivity extends BaseActivity<MainViewModel, MainModel> implements MainContract.View {
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public int getBR() {
        return BR.vm;
    }

    @Override
    public void initView() {
//        //隐藏系统自带的状态栏
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        //隐藏掉整个ActionBar
//        if (getSupportActionBar() != null)
//            getSupportActionBar().hide();

//        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_container);
//        BindingRecyclerViewAdapters.setAdapter(
//                recyclerView,
//                ItemBinding.<MainViewModel>of(BR.user, R.layout.item_user),
//                getViewModel().getUserList(),
//                new BindingRecyclerViewAdapter<MainViewModel>(),
//                null,
//                null
//        );
//        BindingRecyclerViewAdapters.setLayoutManager(recyclerView, LayoutManagers.linear());
//        getViewModel().loadData();

        ActivityMainBinding binding = (ActivityMainBinding) getViewDataBinding();
        binding.viewPager.setAdapter(new MainAdapter(this));
        binding.viewPager.setOffscreenPageLimit(4);
        binding.alphaIndicator.setViewPager(binding.viewPager);
        //----------------------------------------------------------
        //如果是第一次使用添加用户引导
        Boolean isFirstUse = (Boolean) ACache.get(this).getAsObject("isFirstUse");
        if (isFirstUse == null || isFirstUse) {
            ACache.get(this).put("isFirstUse", Boolean.FALSE);
            runTourGuide();
        }
        setSlideBack(false);

//        new Thread(() -> {
//            try {
//                MailUtils.sendMail("哟见");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }).start();

        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("demo-pool-%d")
                .build();
        ExecutorService singleThreadPool = new ThreadPoolExecutor(1,
                1,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingDeque<Runnable>(1024),
                namedThreadFactory,
                new ThreadPoolExecutor.AbortPolicy());
        singleThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                Logger.d(Thread.currentThread().getName());
                System.out.print(Thread.currentThread().getName());
            }
        });
        singleThreadPool.shutdown();
        final DataProtocol data = new DataProtocol();
        data.setData(DeviceUtil.getDeviceId2());
    }

    /**
     * 新手引导页
     */
    private void runTourGuide() {
        Animation mEnterAnimation = new AlphaAnimation(0f, 1f);
        mEnterAnimation.setDuration(600);
        mEnterAnimation.setFillAfter(true);

        Animation mExitAnimation = new AlphaAnimation(1f, 0f);
        mExitAnimation.setDuration(600);
        mExitAnimation.setFillAfter(true);
        //第一步引导
        ChainTourGuide tourGuide1 = ChainTourGuide.init(this)
                .setToolTip(new ToolTip()
                        .setTitle("产品页面")
                        .setDescription("点击产品去产品页")
                        .setGravity(Gravity.TOP)
                )
                .playLater(findViewById(R.id.av_product));
        //第二步引导
        ChainTourGuide tourGuide2 = ChainTourGuide.init(this)
                .setToolTip(new ToolTip()
                        .setTitle("发现页面")
                        .setDescription("点击发现去发现页")
                        .setGravity(Gravity.TOP | Gravity.START)
                        .setBackgroundColor(Color.parseColor("#c0392b"))
                )
                .setOverlay(new Overlay()
                        .setBackgroundColor(Color.parseColor("#EE2c3e50"))
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation)
                )
                .playLater(findViewById(R.id.av_discover));
        //第三步引导
        ChainTourGuide tourGuide3 = ChainTourGuide.init(this)
                .setToolTip(new ToolTip()
                        .setTitle("我的页面")
                        .setDescription("点击我的去我的页")
                        .setGravity(Gravity.TOP)
                )
                .playLater(findViewById(R.id.av_mine));
        //引导序列
        Sequence sequence = new Sequence.SequenceBuilder()
                .add(tourGuide1, tourGuide2, tourGuide3)
                .setDefaultOverlay(new Overlay()
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation))
                .setDefaultPointer(null)
                .setContinueMethod(Sequence.ContinueMethod.Overlay)
                .build();
        //执行引导
        ChainTourGuide.init(this).playInSequence(sequence);
    }

    public void goLogin(View view) {
        startActivity(LoginActivity.class);
    }

    public void goFlycoTabLayout(View view) {
        FlycoTabLayoutActivity.Companion.startActivity(this);
    }

    public void goSegmentTabActivity(View view) {
        SegmentTabActivity.Companion.startActivity(this);
    }

    public void goVLayout(View view) {
        VLayoutActivity.Companion.startActivity(this);
    }

    public void goUpload(View view) {
        UploadActivity.Companion.startActivity(this);
    }

    public void goWeb(View view) {
        WebActivity.Companion.startActivity(this);
    }

    public void goUpdate(View view) {
        UpdateActivity.Companion.startActivity(this);
    }

    public void goDragPhoto(View view) {
        DragPhotoMainActivity.Companion.startActivity(this);
    }

    public void goReplugin(View view) {
        RepluginActivity.Companion.startActivity(this);
    }

    public void goProvider(View view) {
        ProviderActivity.Companion.startActivity(this);
    }

    public void goConstraint(View view) {
        ConstraintActivity.Companion.startActivity(this);
    }

    public void goSnow(View view) {
        SnowActivity.startActivity(this);
    }

    public void goWifi(View view) {
        AutoConnWifiActivity.Companion.startActivity(this);
    }

    public void goTTS(View view) {
        TTSActivity.Companion.startActivity(this);
    }

    public static <VM extends BaseViewModel, M extends BaseModel> void startActivity(BaseActivity<VM, M> activity) {
        activity.startActivity(MainActivity.class);
    }
}
