package com.bobbygu.bobbyapp.function.main.mine

import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.base.BaseLazyFragment
import com.daimajia.swipe.SwipeLayout


@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
/**
 * 我的页面
 * # time:   2017/10/25 19:57
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class MineFragment<VM : BaseViewModel<*, *>, M : BaseModel<*>> : BaseLazyFragment<VM, M>() {
    override fun getLayoutId(): Int = R.layout.fragment_mine

    override fun initView() {
        val sample1 = mViewDataBinding.root.findViewById<SwipeLayout>(R.id.sample1)
        sample1.showMode = SwipeLayout.ShowMode.PullOut
        val starBottView = sample1.findViewById<LinearLayout>(R.id.starbott)
        sample1.addDrag(SwipeLayout.DragEdge.Left, sample1.findViewById(R.id.bottom_wrapper))
        sample1.addDrag(SwipeLayout.DragEdge.Right, sample1.findViewById(R.id.bottom_wrapper_2))
        sample1.addDrag(SwipeLayout.DragEdge.Top, starBottView)
        sample1.addDrag(SwipeLayout.DragEdge.Bottom, starBottView)
        sample1.addRevealListener(R.id.delete) { child, edge, fraction, distance -> }

        sample1.surfaceView.setOnClickListener {
            Toast.makeText(context, "Click on surface", Toast.LENGTH_SHORT).show()
            Log.d(MineFragment::class.java.name, "click on surface")
        }
        sample1.surfaceView.setOnLongClickListener {
            Toast.makeText(context, "longClick on surface", Toast.LENGTH_SHORT).show()
            Log.d(MineFragment::class.java.name, "longClick on surface")
            true
        }
        sample1.findViewById<View>(R.id.star2).setOnClickListener { Toast.makeText(context, "Star", Toast.LENGTH_SHORT).show() }

        sample1.findViewById<View>(R.id.trash2).setOnClickListener { Toast.makeText(context, "Trash Bin", Toast.LENGTH_SHORT).show() }

        sample1.findViewById<View>(R.id.magnifier2).setOnClickListener { Toast.makeText(context, "Magnifier", Toast.LENGTH_SHORT).show() }

        sample1.addRevealListener(R.id.starbott) { child, edge, fraction, distance ->
            val star = child.findViewById<View>(R.id.star)
            val d = child.height / 2 - star.height / 2
            star.translationY = d * fraction
            star.scaleX = fraction + 0.6f
            star.scaleY = fraction + 0.6f
        }
    }

    companion object {
        private val BUNDLE_TITLE = "title"
        fun <VM : BaseViewModel<*, *>, M : BaseModel<*>> newInstance(): MineFragment<VM, M> {
//            val bundle = Bundle()
//            bundle.putString(BUNDLE_TITLE, title)
            val fragment = MineFragment<VM, M>()
//            fragment.arguments = bundle
            return fragment
        }
    }
}
