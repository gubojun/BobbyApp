package com.bobbygu.bobbyapp.function.main.home

import android.databinding.ObservableArrayList
import com.bobbygu.libnet.common.ApiConstants
import com.bobbygu.libnet.common.HostType
import com.bobbygu.bobbyapp.bean.AdPicBean

/**
 * 首页
 * # time:   2017/11/7 12:17
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class HomeViewModel : HomeContract.ViewModel() {
    private val adPicList = ObservableArrayList<AdPicBean>()
    override fun returnAdList(adPicList: List<AdPicBean>) {
        this.adPicList.clear()
        this.adPicList.addAll(adPicList)
        val list = adPicList.map { adPicBean ->
            "${ApiConstants.getHost(HostType.PRODUCT_HOST)}/${adPicBean.imgUrl}/${adPicBean.name}"
        } as ArrayList<String>
        view.initAD(list)
    }

    override fun init() = getAdList()

    override fun getAdList() = model.getAdList()
}