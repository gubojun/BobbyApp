package com.bobbygu.bobbyapp.function

import android.graphics.Color
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel
import com.bobbygu.libnet.utils.DeviceUtil.dp2px
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.utils.ViewFindUtils
import com.flyco.tablayout.CommonTabLayout
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.flyco.tablayout.utils.UnreadMsgUtils
import java.util.*


@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
class FlycoTabLayoutActivity<VM : BaseViewModel<*, *>, M : BaseModel<*>> : BaseActivity<VM, M>() {
    private val mFragments = ArrayList<Fragment>()
    private val mFragments2 = ArrayList<Fragment>()
    private val mTitles = arrayOf("首页", "消息", "联系人", "更多")
    private val mIconUnselectIds = intArrayOf(R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher)
    private val mIconSelectIds = intArrayOf(R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round)
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private var mDecorView: View? = null
    private var mViewPager: ViewPager? = null
    private var mTabLayout_1: CommonTabLayout? = null
    private var mTabLayout_2: CommonTabLayout? = null
    private var mTabLayout_3: CommonTabLayout? = null
    private var mTabLayout_4: CommonTabLayout? = null
    private var mTabLayout_5: CommonTabLayout? = null
    private var mTabLayout_6: CommonTabLayout? = null
    private var mTabLayout_7: CommonTabLayout? = null
    private var mTabLayout_8: CommonTabLayout? = null

    override fun getLayoutId() = R.layout.activity_flyco_tab_layout

    override fun initView() {
        for (title in mTitles) {
            mFragments.add(SimpleCardFragment.getInstance("Switch ViewPager " + title))
            mFragments2.add(SimpleCardFragment.getInstance("Switch Fragment " + title))
        }
        for (i in 0 until mTitles.size) {
            mTabEntities.add(TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]))
        }
        mDecorView = window.decorView
        mViewPager = ViewFindUtils.find(mDecorView, R.id.vp_2)
        mViewPager?.adapter = MyPagerAdapter(supportFragmentManager)

        /** with nothing */
        mTabLayout_1 = ViewFindUtils.find(mDecorView, R.id.tl_1)
        /** with ViewPager */
        mTabLayout_2 = ViewFindUtils.find(mDecorView, R.id.tl_2)
        /** with Fragments */
        mTabLayout_3 = ViewFindUtils.find(mDecorView, R.id.tl_3)
        /** indicator固定宽度 */
        mTabLayout_4 = ViewFindUtils.find(mDecorView, R.id.tl_4)
        /** indicator固定宽度 */
        mTabLayout_5 = ViewFindUtils.find(mDecorView, R.id.tl_5)
        /** indicator矩形圆角 */
        mTabLayout_6 = ViewFindUtils.find(mDecorView, R.id.tl_6)
        /** indicator三角形 */
        mTabLayout_7 = ViewFindUtils.find(mDecorView, R.id.tl_7)
        /** indicator圆角色块 */
        mTabLayout_8 = ViewFindUtils.find(mDecorView, R.id.tl_8)

        mTabLayout_1?.setTabData(mTabEntities)
        tl_2()
        mTabLayout_3?.setTabData(mTabEntities, this, R.id.fl_change, mFragments2)
        mTabLayout_4?.setTabData(mTabEntities)
        mTabLayout_5?.setTabData(mTabEntities)
        mTabLayout_6?.setTabData(mTabEntities)
        mTabLayout_7?.setTabData(mTabEntities)
        mTabLayout_8?.setTabData(mTabEntities)
        mTabLayout_3?.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                mTabLayout_1?.currentTab = position
                mTabLayout_2?.currentTab = position
                mTabLayout_4?.currentTab = position
                mTabLayout_5?.currentTab = position
                mTabLayout_6?.currentTab = position
                mTabLayout_7?.currentTab = position
                mTabLayout_8?.currentTab = position
            }

            override fun onTabReselect(position: Int) {
            }
        })
        mTabLayout_8?.currentTab = 2
        mTabLayout_3?.currentTab = 1

        //显示未读红点
        mTabLayout_1?.showDot(2)
        mTabLayout_3?.showDot(1)
        mTabLayout_4?.showDot(1)

        //两位数
        mTabLayout_2?.showMsg(0, 55)
        mTabLayout_2?.setMsgMargin(0, -5f, 5f)

        //三位数
        mTabLayout_2?.showMsg(1, 100)
        mTabLayout_2?.setMsgMargin(1, -5f, 5f)

        //设置未读消息红点
        mTabLayout_2?.showDot(2)
        val rtv_2_2 = mTabLayout_2?.getMsgView(2)
        if (rtv_2_2 != null) {
            UnreadMsgUtils.setSize(rtv_2_2, dp2px(mContext, 7.5f))
        }

        //设置未读消息背景
        mTabLayout_2?.showMsg(3, 5)
        mTabLayout_2?.setMsgMargin(3, 0f, 5f)
        val rtv_2_3 = mTabLayout_2?.getMsgView(3)
        if (rtv_2_3 != null) {
            rtv_2_3.backgroundColor = Color.parseColor("#6D8FB0")
        }
    }


    private fun tl_2() {
        val mRandom = Random()
        mTabLayout_2?.setTabData(mTabEntities)
        mTabLayout_2?.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                mViewPager?.currentItem = position
            }

            override fun onTabReselect(position: Int) {
                if (position == 0) {
                    mTabLayout_2?.showMsg(0, mRandom.nextInt(100) + 1)
                    //                    UnreadMsgUtils.show(mTabLayout_2.getMsgView(0), mRandom.nextInt(100) + 1);
                }
            }
        })

        mViewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                mTabLayout_2?.currentTab = position
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        mViewPager?.currentItem = 1
    }

    inner class TabEntity(private var title: String,
                          private var selectedIcon: Int,
                          private var unSelectedIcon: Int) : CustomTabEntity {
        override fun getTabTitle() = title
        override fun getTabSelectedIcon() = selectedIcon
        override fun getTabUnselectedIcon() = unSelectedIcon
    }

    private inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getCount() = mFragments.size
        override fun getPageTitle(position: Int) = mTitles[position]
        override fun getItem(position: Int) = mFragments[position]
    }


    companion object {
        fun <VM : BaseViewModel<*, *>, M : BaseModel<*>> startActivity(activity: BaseActivity<VM, M>) {
            activity.startActivity(FlycoTabLayoutActivity::class.java)
        }
    }
}
