package com.bobbygu.bobbyapp.function.update

import android.view.View
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.bobbyapp.R
import ezy.boost.update.UpdateManager
import kotlinx.android.synthetic.main.activity_update.*
import android.widget.Toast
import ezy.boost.update.UpdateUtil
import ezy.boost.update.UpdateInfo


/**
 * UpdateActivity
 * # time: 2017/11/27 16:55
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since
 */
class UpdateActivity : BaseActivity<UpdateViewModel, UpdateModel>(), UpdateContract.View, View.OnClickListener {
    var mCheckUrl = "http://client.waimai.baidu.com/message/updatetag"
    var mUpdateUrl = "http://mobile.ac.qq.com/qqcomic_android.apk"

    override fun getLayoutId(): Int = R.layout.activity_update
    override fun initView() {
        UpdateManager.setDebuggable(true)
        UpdateManager.setWifiOnly(false)
        UpdateManager.setUrl(mCheckUrl, "yyb")
        UpdateManager.check(this)

        check_update.setOnClickListener(this)
        check_update_cant_ignore.setOnClickListener(this)
        check_update_force.setOnClickListener(this)
        check_update_no_newer.setOnClickListener(this)
        check_update_silent.setOnClickListener(this)
        clean.setOnClickListener(this)
    }

    private fun check(isManual: Boolean, hasUpdate: Boolean, isForce: Boolean,
                      isSilent: Boolean, isIgnorable: Boolean, notifyId: Int) {
        UpdateManager.create(this).setChecker { agent, url ->
            agent.setInfo("")
        }.setUrl(mCheckUrl).setManual(isManual).setNotifyId(notifyId).setParser {
            val info = UpdateInfo()
            info.hasUpdate = hasUpdate
            info.updateContent = "• 支持文字、贴纸、背景音乐，尽情展现欢乐气氛；\n• 两人视频通话支持实时滤镜，丰富滤镜，多彩心情；\n• 图片编辑新增艺术滤镜，一键打造文艺画风；\n• 资料卡新增点赞排行榜，看好友里谁是魅力之王。"
            info.versionCode = 587
            info.versionName = "v5.8.7"
            info.url = mUpdateUrl
            info.md5 = "56cf48f10e4cf6043fbf53bbbc4009e3"
            info.size = 10149314
            info.isForce = isForce
            info.isIgnorable = isIgnorable
            info.isSilent = isSilent
            info
        }.check()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.check_update -> check(true, true, false, false, true, 998)
            R.id.check_update_cant_ignore -> check(true, true, false, false, false, 998)
            R.id.check_update_force -> check(true, true, true, false, true, 998)
            R.id.check_update_no_newer -> check(true, false, false, false, true, 998)
            R.id.check_update_silent -> check(true, true, false, true, true, 998)
            R.id.clean -> {
                UpdateUtil.clean(this)
                Toast.makeText(this, "cleared", Toast.LENGTH_LONG).show()
            }
        }
    }

    companion object {

        /**
         * @param activity BaseActivity
         */
        fun startActivity(activity: BaseActivity<*, *>) {
            activity.startActivity(UpdateActivity::class.java)
        }
    }
}