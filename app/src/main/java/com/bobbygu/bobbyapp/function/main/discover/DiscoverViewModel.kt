package com.bobbygu.bobbyapp.function.main.discover

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import com.bobbygu.bobbyapp.bean.UserBean
import com.orhanobut.logger.Logger

/**
 *
 * # time:   2017/10/26 9:33
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class DiscoverViewModel : DiscoverContract.ViewModel() {
    val username = ObservableField<String>()
    val userList = ObservableArrayList<UserBean>()
    override fun loadData() {
        model.loadData()
    }

    override fun init() {
    }

    override fun getUserList(): List<UserBean> = userList
    override fun returnUserList(userList: List<UserBean>) {
        Logger.d("returnUserList:" + userList)
        username.set(userList[0].user)
        this.userList.clear()
        this.userList.addAll(userList)
    }
}