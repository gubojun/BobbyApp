package com.bobbygu.bobbyapp.function.login;

import android.view.View;

import com.bobbygu.lib.mvvm.common.BaseActivity;
import com.bobbygu.lib.mvvm.common.BaseModel;
import com.bobbygu.lib.mvvm.common.BaseViewModel;
import com.bobbygu.bobbyapp.BR;
import com.bobbygu.bobbyapp.R;

/**
 * LoginActivity
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/23 12:30 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class LoginActivity extends BaseActivity<LoginViewModel, LoginModel> implements LoginContract.View {
    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
    }

    @Override
    public int getBR() {
        return BR.vm;
    }

    public void doLogin(View view) {
        getViewModel().doLogin();
    }

    public void doRegist(View view) {
        getViewModel().doRegist();
    }

    public static <VM extends BaseViewModel, M extends BaseModel> void startActivity(BaseActivity<VM, M> activity) {
        activity.startActivity(LoginActivity.class);
    }
}
