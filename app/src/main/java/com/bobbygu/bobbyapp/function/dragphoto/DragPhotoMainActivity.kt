package com.bobbygu.bobbyapp.function.dragphoto

import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel
import com.bobbygu.bobbyapp.R

/**
 *
 * # time: 2017/11/27 18:12
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
class DragPhotoMainActivity<VM : BaseViewModel<*, *>, M : BaseModel<*>> : BaseActivity<VM, M>() {

    override fun getLayoutId(): Int = R.layout.activity_drag_photo_main
    override fun initView() {
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    fun onClick(view: View) {
        DragPhotoActivity.startActivity(this, view as ImageView)
    }

    companion object {

        /**
         * @param activity Activity
         */
        fun <VM : BaseViewModel<*, *>, M : BaseModel<*>> startActivity(activity: BaseActivity<VM, M>) {
            activity.startActivity(DragPhotoMainActivity::class.java)
        }
    }
}