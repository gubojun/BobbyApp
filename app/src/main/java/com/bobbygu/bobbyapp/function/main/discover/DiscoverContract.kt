package com.bobbygu.bobbyapp.function.main.discover

import com.bobbygu.lib.mvvm.common.BaseView
import com.bobbygu.bobbyapp.base.MyBaseM
import com.bobbygu.bobbyapp.base.MyBaseVM
import com.bobbygu.bobbyapp.bean.UserBean

/**
 *
 * # time:   2017/10/26 9:32
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
interface DiscoverContract {
    interface View : BaseView

    abstract class ViewModel : MyBaseVM<View, Model>() {
        abstract fun loadData()
        abstract fun returnUserList(userList: List<UserBean>)
        abstract fun getUserList(): List<UserBean>
    }

    abstract class Model : MyBaseM<ViewModel>() {
        abstract fun loadData()
    }
}