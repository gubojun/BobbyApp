package com.bobbygu.bobbyapp.function.update

import com.bobbygu.lib.mvvm.common.BaseView
import com.bobbygu.bobbyapp.base.MyBaseM
import com.bobbygu.bobbyapp.base.MyBaseVM

/**
 * UpdateContract
 * # time: 2017/11/27 16:58
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
interface UpdateContract {
    interface View : BaseView

    abstract class ViewModel : MyBaseVM<View, Model>()

    abstract class Model : MyBaseM<ViewModel>() {
    }
}