package com.bobbygu.bobbyapp.function.main;

import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bobbygu.bobbyapp.base.MyApplication;

import java.util.HashMap;
import java.util.List;

/**
 * MainModel
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/19 11:02 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class MainModel extends MainContract.Model {
    @Override
    public void loadData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("BankId", "23423");
        map.put("LoginType", "P");

        post("showUser", map, new SuccessCallBack() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                List<MainViewModel> list = JSON.parseArray(jsonObject.getJSONArray("data").toJSONString(), MainViewModel.class);
                getViewModel().returnUserList(list);
            }
        }, new ErrorCallBack() {
            @Override
            public void onError(String errMsg, String errCode) {
                Toast.makeText(MyApplication.getInstance(), errMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
