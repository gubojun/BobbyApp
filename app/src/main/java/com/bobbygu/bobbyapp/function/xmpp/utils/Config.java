package com.bobbygu.bobbyapp.function.xmpp.utils;

import com.bobbygu.libnet.common.ApiConstants;
import com.bobbygu.libnet.common.HostType;

/**
 * Config
 * <p>
 * author: 顾博君 <br>
 * time:   2018/2/1 14:38 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class Config {
    /**
     * 协议版本号
     */
    public static final int VERSION = 1;
    /**
     * 服务器地址
     */
    public static final String ADDRESS = ApiConstants.getHost(HostType.PRODUCT_HOST)
            .replaceAll("/.*|\\D+/|:\\d+", "");
    /**
     * 服务器端口号
     */
    public static final int PORT = 8082;
}
