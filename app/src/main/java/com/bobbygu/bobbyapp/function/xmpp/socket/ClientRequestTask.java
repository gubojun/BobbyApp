package com.bobbygu.bobbyapp.function.xmpp.socket;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.bobbygu.bobbyapp.function.xmpp.protocol.BasicProtocol;
import com.bobbygu.bobbyapp.function.xmpp.protocol.DataProtocol;
import com.bobbygu.bobbyapp.function.xmpp.protocol.PingProtocol;
import com.bobbygu.bobbyapp.function.xmpp.utils.Config;
import com.bobbygu.bobbyapp.function.xmpp.utils.SocketUtil;
import com.orhanobut.logger.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.net.SocketFactory;

/**
 * ClientRequestTask 写数据采用死循环，没有数据时wait，有新消息时notify
 * <p>
 * author: 顾博君 <br>
 * time:   2018/2/1 14:35 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class ClientRequestTask implements Runnable {
    private static final int SUCCESS = 100;
    private static final int FAILED = -1;

    private boolean isLongConnection = true;
    private boolean autoReconnect = true;
    private Handler mHandler;
    private SendTask mSendTask = new SendTask();
    private ReciveTask mReciveTask = new ReciveTask();
    private HeartBeatTask mHeartBeatTask = new HeartBeatTask();
    private Future<?> mSendFuture;
    private Future<?> mReciveFuture;
    private Future<?> mHeartBeatFuture;
    private Socket mSocket;

    private boolean isSocketAvailable;
    private boolean closeSendTask;

    private ExecutorService exec = new ThreadPoolExecutor(3,
            3, 60L, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>());

    protected volatile ConcurrentLinkedQueue<BasicProtocol> dataQueue = new ConcurrentLinkedQueue<>();

    public ClientRequestTask(RequestCallBack requestCallBacks) {
        mHandler = new MyHandler(requestCallBacks);
    }

    private static final int CONNECT_TIMEOUT = 30 * 1000;
    private static final int READ_TIMEOUT = 30 * 1000;

    @Override
    public void run() {
        try {
            try {
                Logger.d("address:" + Config.ADDRESS);
                mSocket = SocketFactory.getDefault().createSocket(Config.ADDRESS, Config.PORT);
                mSocket.setKeepAlive(true);
//                mSocket.setSoTimeout(0);
            } catch (ConnectException e) {
                failedMessage(-1, "服务器连接异常，请检查网络");
                return;
            }

            isSocketAvailable = true;

            //开启接收线程
            mReciveTask.inputStream = mSocket.getInputStream();
            mReciveFuture = exec.submit(mReciveTask);

            //开启发送线程
            mSendTask.outputStream = mSocket.getOutputStream();
            mSendFuture = exec.submit(mSendTask);

            //开启心跳线程
            if (isLongConnection) {
                mHeartBeatTask.outputStream = mSocket.getOutputStream();
                mHeartBeatFuture = exec.submit(mHeartBeatTask);
            }
        } catch (IOException e) {
            failedMessage(-1, "网络发生异常，请稍后重试");
            e.printStackTrace();
        }
    }

    public void addRequest(DataProtocol data) {
        dataQueue.add(data);
        toNotifyAll(dataQueue);//有新增待发送数据，则唤醒发送线程
    }

    public synchronized void stop() {
        //关闭接收线程
        closeReciveTask();
        //关闭发送线程
        closeSendTask = true;
        toNotifyAll(dataQueue);
        //关闭心跳线程
        closeHeartBeatTask();
        //关闭socket
        closeSocket();
        //清除数据
        clearData();
        failedMessage(-1, "断开连接");
    }

    /**
     * 关闭接收线程
     */
    private void closeReciveTask() {
        if (mReciveTask != null) {
//            mReciveTask.interrupt();
            mReciveFuture.cancel(true);
            mReciveTask.isCancle = true;
            if (mReciveTask.inputStream != null) {
                try {
                    if (isSocketAvailable && !mSocket.isClosed() && mSocket.isConnected()) {
                        mSocket.shutdownInput();//解决java.net.SocketException问题，需要先shutdownInput
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                SocketUtil.closeInputStream(mReciveTask.inputStream);
                mReciveTask.inputStream = null;
            }
            mReciveTask = null;
        }
    }

    /**
     * 关闭发送线程
     */
    private void closeSendTask() {
        if (mSendTask != null) {
            mSendTask.isCancle = true;
//            mSendTask.interrupt();
            mSendFuture.cancel(true);
            if (mSendTask.outputStream != null) {
                synchronized (mSendTask.outputStream) {//防止写数据时停止，写完再停
                    SocketUtil.closeOutputStream(mSendTask.outputStream);
                    mSendTask.outputStream = null;
                }
            }
            mSendTask = null;
        }
    }

    /**
     * 关闭心跳线程
     */
    private void closeHeartBeatTask() {
        if (mHeartBeatTask != null) {
            mHeartBeatTask.isCancle = true;
            if (mHeartBeatTask.outputStream != null) {
                SocketUtil.closeOutputStream(mHeartBeatTask.outputStream);
                mHeartBeatTask.outputStream = null;
            }
            mHeartBeatTask = null;
        }
    }

    /**
     * 关闭socket
     */
    private void closeSocket() {
        if (mSocket != null) {
            try {
                mSocket.close();
                isSocketAvailable = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 清除数据
     */
    private void clearData() {
        dataQueue.clear();
        isLongConnection = false;
    }

    private void toWait(Object o) {
        synchronized (o) {
            try {
                o.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * notify()调用后，并不是马上就释放对象锁的，而是在相应的synchronized(){}语句块执行结束，自动释放锁后
     *
     * @param o
     */
    protected void toNotifyAll(Object o) {
        synchronized (o) {
            o.notifyAll();
        }
    }

    private void failedMessage(int code, String msg) {
        Message message = mHandler.obtainMessage(FAILED);
        message.what = FAILED;
        message.arg1 = code;
        message.obj = msg;
        mHandler.sendMessage(message);
    }

    private void successMessage(BasicProtocol protocol) {
        Message message = mHandler.obtainMessage(SUCCESS);
        message.what = SUCCESS;
        message.obj = protocol;
        mHandler.sendMessage(message);
    }

    public void resetSocket() {
        while (isServerClose(mSocket)) {
            try {
                mSocket = SocketFactory.getDefault().createSocket(Config.ADDRESS, Config.PORT);
                mReciveTask.inputStream = mSocket.getInputStream();
                mSendTask.outputStream = mSocket.getOutputStream();
                if (isLongConnection) {
                    mHeartBeatTask.outputStream = mSocket.getOutputStream();
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 判断是否断开连接，断开返回true,没有返回false
     *
     * @param socket
     * @return
     */
    public static Boolean isServerClose(Socket socket) {
        try {
            //发送1个字节的紧急数据，默认情况下，服务器端没有开启紧急数据处理，不影响正常通信
            socket.sendUrgentData(0);
            return false;
        } catch (Exception se) {
            return true;
        }
    }

    private boolean isConnected() {
        if (mSocket.isClosed() || !mSocket.isConnected()) {
            Log.d("isConnected()", "isClosed:" + mSocket.isClosed() + "\nisConnected:" + mSocket.isConnected());
//            if (autoReconnect) {
//                resetSocket();
//            } else {
            ClientRequestTask.this.stop();
            return false;
//            }
        }
        return true;
    }

    /**
     * 服务器返回处理，主线程运行
     */
    public class MyHandler extends Handler {

        private RequestCallBack mRequestCallBack;

        public MyHandler(RequestCallBack callBack) {
            super(Looper.getMainLooper());
            this.mRequestCallBack = callBack;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    mRequestCallBack.onSuccess((BasicProtocol) msg.obj);
                    break;
                case FAILED:
                    mRequestCallBack.onFailed(msg.arg1, (String) msg.obj);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 数据接收线程
     */
    public class ReciveTask implements Runnable {

        private boolean isCancle = false;
        private InputStream inputStream;

        @Override
        public void run() {
            while (!isCancle) {
                if (!isConnected()) {
                    break;
                }

                if (inputStream != null) {
                    BasicProtocol reciverData = SocketUtil.readFromStream(inputStream);
                    if (reciverData != null) {
                        if (reciverData.getProtocolType() == 1 || reciverData.getProtocolType() == 3) {
                            successMessage(reciverData);
                        }
                    } else {
                        break;
                    }
                }
            }
            //循环结束则退出输入流
            SocketUtil.closeInputStream(inputStream);
        }
    }

    /**
     * 数据发送线程
     * 当没有发送数据时让线程等待
     */
    public class SendTask implements Runnable {
        private boolean isCancle = false;
        private OutputStream outputStream;

        @Override
        public void run() {
            while (!isCancle) {
                if (!isConnected()) {
                    break;
                }

                BasicProtocol dataContent = dataQueue.poll();
                if (dataContent == null) {
                    //没有发送数据则等待
                    toWait(dataQueue);
                    if (closeSendTask) {
                        closeSendTask();//notify()调用后，并不是马上就释放对象锁的，所以在此处中断发送线程
                    }
                } else if (outputStream != null) {
                    synchronized (outputStream) {
                        SocketUtil.write2Stream(dataContent, outputStream);
                    }
                }
            }
            //循环结束则退出输出流
            SocketUtil.closeOutputStream(outputStream);
        }
    }

    /**
     * 心跳实现，频率5秒
     * Created by meishan on 16/12/1.
     */
    public class HeartBeatTask implements Runnable {

        private static final int REPEATTIME = 30 * 1000;
        private boolean isCancle = false;
        private OutputStream outputStream;
        private int pingId;

        @Override
        public void run() {
            pingId = 1;
            while (!isCancle) {
                Log.d("HeartBeatTask", "pingId:" + pingId);
                if (!isConnected()) {
                    break;
                }
//                try {
//                    mSocket.sendUrgentData(0xFF);
//                } catch (IOException e) {
//                    Log.d("run", "sendUrgentData wrong");
//                    e.printStackTrace();
//                    isSocketAvailable = false;
//                    ClientRequestTask.this.stop();
//                    break;
//                }

                if (outputStream != null) {
                    PingProtocol pingProtocol = new PingProtocol();
                    pingProtocol.setPingId(pingId);
                    pingProtocol.setUnused("ping...");
                    SocketUtil.write2Stream(pingProtocol, outputStream);
                    pingId = pingId + 1;
                }

                try {
                    Thread.sleep(REPEATTIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            SocketUtil.closeOutputStream(outputStream);
        }
    }
}
