package com.bobbygu.bobbyapp.function

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.view.View
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.widget.Button
import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.lib.mvvm.common.BaseModel
import com.bobbygu.lib.mvvm.common.BaseViewModel
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.utils.FileUtils
import com.bobbygu.bobbyapp.utils.ZipUtil
import com.github.lzyzsd.jsbridge.BridgeWebView
import com.github.lzyzsd.jsbridge.DefaultHandler
import com.google.gson.Gson
import com.orhanobut.logger.Logger
import java.io.File
import java.io.FileInputStream


/**
 *
 * # time:   2017/11/13 17:43
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
@Suppress("FINITE_BOUNDS_VIOLATION_IN_JAVA")
class WebActivity<VM : BaseViewModel<*, *>, M : BaseModel<*>> : BaseActivity<VM, M>() {
    private var webView: BridgeWebView? = null

    var button: Button? = null

    private var RESULT_CODE = 0

    var mUploadMessage: ValueCallback<Uri>? = null

    internal class Location {
        var address: String? = null
    }

    internal class User {
        var name: String? = null
        var location: Location? = null
        var testStr: String? = null
    }

    override fun getLayoutId(): Int = R.layout.activity_web

    override fun initView() {
        webView = findViewById(R.id.webView)

        button = findViewById(R.id.button)

        webView?.setDefaultHandler(DefaultHandler())

        webView?.webChromeClient = object : WebChromeClient() {

            fun openFileChooser(uploadMsg: ValueCallback<Uri>, AcceptType: String, capture: String) {
                this.openFileChooser(uploadMsg)
            }

            fun openFileChooser(uploadMsg: ValueCallback<Uri>, AcceptType: String) {
                this.openFileChooser(uploadMsg)
            }

            fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
                mUploadMessage = uploadMsg
                pickFile()
            }
        }
//        val path = "${Environment.getExternalStorageDirectory()}${File.separator}"
//        ZipUtil.unZipFolder(FileInputStream(path + "test.zip"), path)
//        Logger.d("path = $path")
//        webView?.loadUrl(path + "test.html")
        webView?.loadUrl("file:///android_asset/demo.html")

        webView?.registerHandler("submitFromWeb", { data, function ->
            Logger.i("handler = submitFromWeb, data from web = " + data)
            function.onCallBack("submitFromWeb exe, response data 中文 from Java")
        })

        webView?.registerHandler("JavaScriptCall", { data, function ->
            Logger.i("handler = submitFromWeb, data from web = " + data)
            function.onCallBack("submitFromWeb exe, response data 中文 from Java")
        })

        val user = User()
        val location = Location()
        location.address = "SDU"
        user.location = location
        user.name = "大头鬼"

        webView?.callHandler("functionInJs", Gson().toJson(user), { })

        webView?.send("hello")
    }

    fun buttonClick(view: View) {
        if (button!! == view) {
            webView?.callHandler("functionInJs", "data from Java", { data ->
                Logger.i("reponse data from js " + data)
            })
        }
    }

    fun pickFile() {
        val chooserIntent = Intent(Intent.ACTION_GET_CONTENT)
        chooserIntent.type = "image/*"
        startActivityForResult(chooserIntent, RESULT_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (requestCode == RESULT_CODE) {
            if (null == mUploadMessage) {
                return
            }
            val result = if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
            mUploadMessage?.onReceiveValue(result)
            mUploadMessage = null
        }
    }

    companion object {

        /**
         * @param activity BaseActivity
         */
        fun startActivity(activity: BaseActivity<*, *>) {
            activity.startActivity(WebActivity::class.java)
        }
    }
}