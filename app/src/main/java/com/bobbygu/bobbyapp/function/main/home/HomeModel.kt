package com.bobbygu.bobbyapp.function.main.home

import android.widget.Toast
import com.alibaba.fastjson.JSON
import com.bobbygu.bobbyapp.base.MyApplication
import com.bobbygu.bobbyapp.bean.AdPicBean
import com.bobbygu.bobbyapp.network.HttpAction

/**
 * 首页
 * # time:   2017/11/7 12:15
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class HomeModel : HomeContract.Model() {
    override fun getAdList() {
        post(HttpAction.GetHomeAdImgList, { jsonObject ->
            val list = JSON.parseArray(jsonObject.getJSONArray("data").toJSONString(), AdPicBean::class.java)
            viewModel.returnAdList(list)
        }, { errMsg, _ ->
            Toast.makeText(MyApplication.getInstance(), errMsg, Toast.LENGTH_SHORT).show()
        })
    }
}