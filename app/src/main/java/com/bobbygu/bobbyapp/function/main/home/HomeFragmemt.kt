package com.bobbygu.bobbyapp.function.main.home

import com.bobbygu.lib.mvvm.common.BaseActivity
import com.bobbygu.bobbyapp.R
import com.bobbygu.bobbyapp.base.BaseLazyFragment
import com.bobbygu.bobbyapp.view.loopviewpager.ADPagerAdapter
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * 首页
 * # time:   2017/10/26 10:38
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class HomeFragmemt : BaseLazyFragment<HomeViewModel, HomeModel>(), HomeContract.View {
    override fun onUserVisible() {
        mViewModel.getAdList()
    }

    override fun getLayoutId(): Int = R.layout.fragment_home
    override fun initView() {
    }

    companion object {
        fun newInstance() = HomeFragmemt()
    }

    override fun initAD(list: ArrayList<String>) {
        val adPagerAdapter = ADPagerAdapter(activity as BaseActivity<*, *>)
        lvp.initViewPager(adPagerAdapter, list)
        if (list.size > 1) {
            adPagerAdapter.registerDataSetObserver(indicator.dataSetObserver)
            indicator.setViewPager(lvp)
        }
    }
}