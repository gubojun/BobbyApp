package com.bobbygu.bobbyapp.bean
/**
 *
 * # time: 2017/11/28 12:18
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class AdPicBean {
    var name: String? = null
    var imgUrl: String? = null
}