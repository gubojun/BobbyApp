package com.bobbygu.bobbyapp.bean

/**
 *
 * # time: 2017/11/29 16:01
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class UserBean {
    var user: String? = null
    var password: String? = null
    var token: String? = null
    var sign: String? = null
    var age: Int = 0

    override fun toString(): String {
        return "UserBean(user=$user, password=$password, token=$token, sign=$sign, age=$age)"
    }
}