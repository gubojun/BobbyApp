package com.bobbygu.bobbyapp.bean

/**
 * PluginBean
 * # time: 2017/11/30 16:11
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class PluginBean {
    var name: String? = null
    var fileUrl: String? = null
    var buttonName: String? = null
    override fun toString(): String {
        return "PluginBean(name=$name, fileUrl=$fileUrl)"
    }
}