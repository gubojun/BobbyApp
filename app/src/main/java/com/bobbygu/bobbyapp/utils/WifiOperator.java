package com.bobbygu.bobbyapp.utils;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

/**
 * Created by BobbyGu on 2018/1/25.
 */

public class WifiOperator {
    private static Handler mHandler;
    private static Context context;
    private WifiManager wifiManager;

    private static WifiOperator wifiOperator;

    private WifiManager.WifiLock wifiLock;

    private List<WifiConfiguration> wifiConfigurationList;
    private List<ScanResult> scanResultList;

    private WifiOperator() {
        if (context != null) {
            wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            this.startScan();
        }
    }

    public static WifiOperator getInstance() {
        if (wifiOperator == null && context != null) {
            wifiOperator = new WifiOperator();
        }
        return wifiOperator;
    }

    public static void setHandler(Handler handler) {
        mHandler = handler;
    }

    public static void setContext(Context _context) {
        context = _context;
    }

    /**
     * 查看WIFI当前是否处于打开状态
     *
     * @return false 处于打开状态；true 处于非打开状态(包括UnKnow状态)。
     */
    public boolean isWifiClosed() {
        int wifiState = getWifiState();
        if (wifiState == WifiManager.WIFI_STATE_DISABLED || wifiState == WifiManager.WIFI_STATE_DISABLING) {
            sendMsg("当前Wifi状态:关闭或异常");
            return true;
        }
        sendMsg("当前Wifi状态:开启");
        return false;
    }

    /**
     * 查看WIFI当前是否处于关闭状态
     *
     * @return false 处于关闭状态；true 处于非关闭状态(包括UNKNOW状态)
     */
    public boolean isWifiOpened() {
        int wifiState = getWifiState();
        if (wifiState == WifiManager.WIFI_STATE_ENABLED || wifiState == WifiManager.WIFI_STATE_ENABLING) {
            sendMsg("当前Wifi状态:开启");
            return true;
        }
        sendMsg("当前Wifi状态:关闭或异常");
        return false;
    }

    /**
     * 如果WIFI当前处于关闭状态，则打开WIFI
     */
    public void openWifi() {
        if (wifiManager != null && isWifiClosed()) {
            wifiManager.setWifiEnabled(true);
            sendMsg("Wifi开启成功");
        }
    }

    /**
     * 如果WIFI当前处于打开状态，则关闭WIFI
     */
    public void closeWifi() {
        if (wifiManager != null && isWifiOpened()) {
            wifiManager.setWifiEnabled(false);
            sendMsg("Wifi关闭成功");
        }
    }

    /**
     * 获取当前Wifi的状态编码
     *
     * @return WifiManager.WIFI_STATE_ENABLED，WifiManager.WIFI_STATE_ENABLING，
     * WifiManager.WIFI_STATE_DISABLED，WifiManager.WIFI_STATE_DISABLING，
     * WifiManager.WIFI_STATE_UnKnow 中间的一个
     */
    public int getWifiState() {
        if (wifiManager != null) {
            return wifiManager.getWifiState();
        }
        return 0;
    }

    /**
     * 获取已经配置好的Wifi网络
     *
     * @return
     */
    public List<WifiConfiguration> getSavedWifiConfiguration() {
        return wifiConfigurationList;
    }

    /**
     * 获取扫描到的网络的信息
     *
     * @return
     */
    public List<ScanResult> getWifiScanResult() {
        return scanResultList;
    }

    /**
     * 执行一次Wifi的扫描
     */
    public synchronized void startScan() {
        if (wifiManager != null) {
            wifiManager.startScan();
            scanResultList = wifiManager.getScanResults();
            wifiConfigurationList = wifiManager.getConfiguredNetworks();
        }
    }

    /**
     * 通过netWorkId来连接一个已经保存好的Wifi网络
     *
     * @param netWorkId
     */
    public void connetionConfiguration(int netWorkId) {
        if (configurationNetWorkIdCheck(netWorkId) && wifiManager != null) {
            boolean enabled = wifiManager.enableNetwork(netWorkId, true);
            sendMsg("enableNetwork status enable=" + enabled);
        }
    }

    /**
     * 断开一个指定ID的网络
     */
    public void disconnectionConfiguration(int netWorkId) {
        wifiManager.disableNetwork(netWorkId);
        wifiManager.disconnect();
    }

    /**
     * 检测尝试连接某个网络时，查看该网络是否已经在保存的队列中间
     *
     * @param netWorkId
     * @return
     */
    private boolean configurationNetWorkIdCheck(int netWorkId) {
        for (WifiConfiguration temp : wifiConfigurationList) {
            if (temp.networkId == netWorkId) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取Wifi的数据
     *
     * @return
     */
    public WifiInfo getWifiConnectionInfo() {
        return wifiManager.getConnectionInfo();
    }

    /**
     * 锁定WIFI，使得在熄屏状态下，仍然可以使用WIFI
     */
    public void acquireWifiLock() {
        if (wifiLock != null) {
            wifiLock.acquire();
        }
    }

    /**
     * 解锁WIFI
     */
    public void releaseWifiLock() {
        if (wifiLock != null) {
            if (wifiLock.isHeld()) {
                wifiLock.acquire();
            }
        }
    }

    /**
     * 创建一个WifiLock
     */
    public void createWifiLock() {
        if (wifiManager != null) {
            wifiLock = wifiManager.createWifiLock("wifiLock");
        }
    }

    /**
     * 保存一个新的网络
     *
     * @param _wifiConfiguration
     */
    public int addNetWork(WifiConfiguration _wifiConfiguration) {
        int netWorkId = -255;
        if (_wifiConfiguration != null && wifiManager != null) {
            netWorkId = wifiManager.addNetwork(_wifiConfiguration);
            startScan();
        }
        return netWorkId;
    }

    /**
     * 保存并连接到一个新的网络
     *
     * @param _wifiConfiguration
     */
    public void addNetWorkAndConnect(WifiConfiguration _wifiConfiguration) {
        int netWorkId = addNetWork(_wifiConfiguration);
        if (netWorkId != -255) {
            connetionConfiguration(netWorkId);
        }
    }

    /**
     * 获取当前连接状态中的Wifi的信号强度
     *
     * @return
     */
    public int getConnectedWifiLevel() {
        WifiInfo wifiInfo = getWifiConnectionInfo();
        if (wifiInfo != null) {
            String connectedWifiSSID = wifiInfo.getSSID();
            if (scanResultList != null) {
                for (ScanResult temp : scanResultList) {
                    if (temp.SSID.replace("\"", "").equals(connectedWifiSSID.replace("\"", ""))) {
                        return temp.level;
                    }
                }
            }
        }
        return 1;
    }

    /**
     * 删除一个已经保存的网络
     *
     * @param netWorkId
     */
    public void remoteNetWork(int netWorkId) {
        if (wifiManager != null) {
            wifiManager.removeNetwork(netWorkId);
        }
    }

    /**
     * Created by wangzong on 14-9-24.
     * <p>
     * Wifi加密类型的描述类
     */
    public enum WifiCipherType {
        NONE, IEEE8021XEAP, WEP, WPA, WPA2
    }

    /**
     * 连接一个WIFI
     *
     * @param ssid
     * @param password
     * @param wifiCipherType
     */
    public void addNetWorkAndConnect(String ssid, String password, WifiCipherType wifiCipherType) {
        if (wifiManager != null && wifiCipherType != null) {
            WifiConfiguration wifiConfig = createWifiConfiguration(ssid, password, wifiCipherType);
            WifiConfiguration temp = isWifiConfigurationSaved(wifiConfig);
            if (temp != null) {
                wifiManager.removeNetwork(temp.networkId);
            } else {
                sendMsg("wifiConfig is null!");
                return;
            }
            addNetWorkAndConnect(wifiConfig);
        }
    }

    public void autoConnect(String ssid, String pass, WifiCipherType wifiCipherType) {
        Thread thread = new Thread(new ConnectRunnable(ssid, pass, wifiCipherType));
        thread.start();
//        addNetWorkAndConnect(ssid, pass, wifiCipherType);
    }

    class ConnectRunnable implements Runnable {
        private String ssid;
        private String password;
        private WifiCipherType type;

        private ConnectRunnable(String ssid, String password, WifiCipherType type) {
            this.ssid = ssid;
            this.password = password;
            this.type = type;
        }

        @Override
        public void run() {
            try {
                // 打开wifi
                openWifi();
                Thread.sleep(200);
                // 开启wifi功能需要一段时间(我在手机上测试一般需要1-3秒左右)，所以要等到wifi
                // 状态变成WIFI_STATE_ENABLED的时候才能执行下面的语句
                while (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
                    try {
                        // 为了避免程序一直while循环，让它睡个100毫秒检测……
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        sendMsg(e.getMessage());
                    }
                }
                addNetWorkAndConnect(ssid, password, type);
//                boolean connected = wifiManager.reconnect();
//                sendMsg("enableNetwork connected=" + connected);
                sendMsg("连接成功!");
            } catch (Exception e) {
                sendMsg(e.getMessage());
                e.printStackTrace();
            }
        }
    }


    /**
     * 向UI发送消息
     *
     * @param info 消息
     */
    public void sendMsg(String info) {
        if (mHandler != null) {
            Message msg = new Message();
            msg.obj = info;
            mHandler.sendMessage(msg);// 向Handler发送消息
        } else {
            Log.e("wifi", info);
        }
    }


    private WifiConfiguration isWifiConfigurationSaved(WifiConfiguration wifiConfig) {
        if (wifiConfigurationList == null) {
            this.startScan();
        }
        for (WifiConfiguration temp : wifiConfigurationList) {
            if (temp.SSID.equals(wifiConfig.SSID)) {
                return temp;
            }
        }
        return null;
    }

    private WifiConfiguration createWifiConfiguration(String ssid, String password, WifiCipherType type) {
        WifiConfiguration newWifiConfiguration = new WifiConfiguration();
        newWifiConfiguration.allowedAuthAlgorithms.clear();
        newWifiConfiguration.allowedGroupCiphers.clear();
        newWifiConfiguration.allowedKeyManagement.clear();
        newWifiConfiguration.allowedPairwiseCiphers.clear();
        newWifiConfiguration.allowedProtocols.clear();
        newWifiConfiguration.SSID = "\"" + ssid + "\"";
        switch (type) {
            case NONE:
                newWifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                break;
            case IEEE8021XEAP:
                break;
            case WEP:
                if (!TextUtils.isEmpty(password)) {
                    if (isHexWepKey(password)) {
                        newWifiConfiguration.wepKeys[0] = password;
                    } else {
                        newWifiConfiguration.wepKeys[0] = "\"" + password + "\"";
                    }
                }
                newWifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                newWifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                newWifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                newWifiConfiguration.wepTxKeyIndex = 0;
                break;
            case WPA:
                newWifiConfiguration.preSharedKey = "\"" + password + "\"";
                newWifiConfiguration.hiddenSSID = true;
                newWifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                newWifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                newWifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                newWifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                newWifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                newWifiConfiguration.status = WifiConfiguration.Status.ENABLED;
                break;
            case WPA2:
                newWifiConfiguration.preSharedKey = "\"" + password + "\"";
                newWifiConfiguration.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                newWifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                newWifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                newWifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                newWifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                newWifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                newWifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                newWifiConfiguration.status = WifiConfiguration.Status.ENABLED;
                break;
            default:
                return null;
        }
        return newWifiConfiguration;
    }

    private static boolean isHexWepKey(String wepKey) {
        final int len = wepKey.length();

        // WEP-40, WEP-104, and some vendors using 256-bit WEP (WEP-232?)
        if (len != 10 && len != 26 && len != 58) {
            return false;
        }

        return isHex(wepKey);
    }

    private static boolean isHex(String key) {
        for (int i = key.length() - 1; i >= 0; i--) {
            final char c = key.charAt(i);
            if (!(c >= '0' && c <= '9' || c >= 'A' && c <= 'F' || c >= 'a'
                    && c <= 'f')) {
                return false;
            }
        }
        return true;
    }
}
