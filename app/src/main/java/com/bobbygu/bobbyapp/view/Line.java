package com.bobbygu.bobbyapp.view;

/**
 * Created by BobbyGu on 2017/12/28.
 */

public class Line {
    int y1;
    int y2;
    int x1;
    int x2;

    public void set(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }
}
