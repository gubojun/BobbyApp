package com.bobbygu.bobbyapp.view.showview;

import java.util.Random;

public class RandomGenerator {
    private static final Random RANDOM = new Random();

    public float getRandom(float lower, float upper) {
        float min = Math.min(lower, upper);
        float max = Math.max(lower, upper);
        return getRandom(max - min) + min;
    }

    public float getRandom(float upper) {
        return RANDOM.nextFloat() * upper;
    }

    public int getRandom(int upper) {
        return RANDOM.nextInt(upper);
    }
    
}

