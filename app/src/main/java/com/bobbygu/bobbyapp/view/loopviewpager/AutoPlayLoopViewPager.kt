package com.bobbygu.bobbyapp.view.loopviewpager

import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.MotionEvent
import java.lang.ref.WeakReference
import java.util.*

/**
 * 自动轮播的ViewPager
 * # time:   2017/10/27 10:57
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.1.4
 */
class AutoPlayLoopViewPager : LoopViewPager {
    private var adPagerAdapter: ADPagerAdapter? = null
    private val WHAT_AUTO_PLAY = 1000
    //是否只有一张图片
    private var mIsOneImg = false
    //是否可以自动播放
    private var mAutoPlayAble = true
    //是否正在播放
    private var mIsAutoPlaying = false
    //自动播放时间
    private val mAutoPalyTime = 5000

    private val mAutoPlayHandler = AutoPlayHandler(WeakReference(this))

    class AutoPlayHandler constructor(private val weakReference: WeakReference<AutoPlayLoopViewPager>) : Handler() {

        override fun handleMessage(msg: Message) {
            val viewpager = weakReference.get()
            if (viewpager != null) {
                viewpager.currentItem = toRealPosition(viewpager.currentItem + 2, viewpager.adPagerAdapter!!.count)
                this.sendEmptyMessageDelayed(viewpager.WHAT_AUTO_PLAY, viewpager.mAutoPalyTime.toLong())
            }
        }
    }


    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    @Suppress("UNUSED")
    fun setAutoPlay(autoPlay: Boolean) {
        mAutoPlayAble = autoPlay
    }

    fun initViewPager(adPagerAdapter: ADPagerAdapter, list: ArrayList<String>) {
        this.adPagerAdapter = adPagerAdapter
        adPagerAdapter.refreshItemList(list)
        this.adapter = adPagerAdapter
        this.clearOnPageChangeListeners()
        //当图片多于1张时添加指示点
//        if (!mIsOneImg) {
//            addPoints()
//        }
        mIsOneImg = list.size <= 1
        //当图片多于1张时开始轮播
        if (!mIsOneImg) {
            startAutoPlay()
        }
    }

    override fun onTouchEvent(arg0: MotionEvent) = !mIsOneImg && super.onTouchEvent(arg0)
    override fun onInterceptTouchEvent(arg0: MotionEvent) = !mIsOneImg && super.onInterceptTouchEvent(arg0)
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (mAutoPlayAble && !mIsOneImg) {
            when (ev.action) {
                MotionEvent.ACTION_DOWN -> stopAutoPlay()
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_OUTSIDE -> startAutoPlay()
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    /**
     * 开始播放
     */
    private fun startAutoPlay() {
        if (mAutoPlayAble && !mIsAutoPlaying) {
            mIsAutoPlaying = true
            mAutoPlayHandler.sendEmptyMessageDelayed(WHAT_AUTO_PLAY, mAutoPalyTime.toLong())
        }
    }

    /**
     * 停止播放
     */
    private fun stopAutoPlay() {
        if (mAutoPlayAble && mIsAutoPlaying) {
            mIsAutoPlaying = false
            mAutoPlayHandler.removeMessages(WHAT_AUTO_PLAY)
        }
    }
}