package com.bobbygu.bobbyapp.network

/**
 *
 * # time:   2017/11/8 21:08
 * # e-mail: gubojun@csii.com.cn
 * @author 顾博君
 * @since 1.0
 */
class HttpAction {
    companion object {
        val GetHomeAdImgList = "GetHomeAdImgList.do"//获取广告列表
        val UploadAppLog = "UploadAppLog.do"//上传日志
        val GetPluginMsg = "GetPluginMsg.do"//获取插件信息
        val ShowUser = "showUser"
        val upload = "upload"
        val GetImg = "getImg.do"
        val Login = "login"
        val Register = "register"
    }
}