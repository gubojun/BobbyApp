package com.bobbygu.bobbyapp.base;

import com.bobbygu.lib.mvvm.common.BaseView;
import com.bobbygu.lib.mvvm.common.BaseViewModel;
import com.bobbygu.libnet.baserx.RxManager;

/**
 * MyBaseVM
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/19 14:50 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public abstract class MyBaseVM<V extends BaseView, M extends MyBaseM> extends BaseViewModel<V, M> {
    public RxManager mRxManager = new RxManager();

    @Override
    public void attachView(V view) {
        super.attachView(view);
        getModel().init(mRxManager);
    }

    @Override
    public void detachView() {
        mRxManager.clear();
        super.detachView();
    }
}
