package com.bobbygu.bobbyapp.base;

/**
 * Constant 常量
 * <p>
 * author: 顾博君 <br>
 * time:   2017/11/8 14:15 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class Constant {
    public static final String COMMON_LOG_TAG = "BOBBY_LOG";
}
