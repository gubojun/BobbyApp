package com.bobbygu.bobbyapp.base;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bobbygu.bobbyapp.R;
import com.squareup.picasso.Picasso;

/**
 * ImageViewAttrAdapter
 * <p>
 * author: 顾博君 <br>
 * time:   2017/11/29 18:59 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class ImageViewAttrAdapter {
    @BindingAdapter("android:src")
    public static void setSrc(ImageView view, Bitmap bitmap) {
        view.setImageBitmap(bitmap);
    }

    @BindingAdapter("android:src")
    public static void setSrc(ImageView view, int resId) {
        view.setImageResource(resId);
    }

    @BindingAdapter("android:src")
    public static void setSrc(ImageView view, String url) {
        Picasso.with(view.getContext()).load(url).placeholder(R.drawable.ic_launcher).fit().into(view);
    }
}
