package com.bobbygu.libnet.bean;

/**
 * DataResponse
 * <p>
 * author: 顾博君 <br>
 * time:   2016/12/22 9:08 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class DataResponse<T> {
    public static final int STATUS_OK = 1000;
    private String desc;
    private int status;
    private T resultMap;

    public T getResultMap() {
        return resultMap;
    }

    public void setResultMap(T resultMap) {
        this.resultMap = resultMap;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
