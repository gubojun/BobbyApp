package com.bobbygu.libnet.baserx;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * RxManager
 * 用于管理单个presenter的RxBus的事件和Rxjava相关代码的生命周期处理
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/3 13:25 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 *
 * @since 1.1.1
 */

public class RxManager {
    public RxBus mRxBus = RxBus.getInstance();
    //管理rxbus订阅
    private Map<String, Observable<?>> mObservables = new HashMap<>();
    /*管理Observables 和 Subscribers订阅*/
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    /**
     * RxBus注入监听
     *
     * @param eventName 事件名称
     * @param consumer  事件消费者
     */
    public <T> void on(String eventName, Consumer<T> consumer) {
        Observable<T> mObservable = mRxBus.register(eventName);
        mObservables.put(eventName, mObservable);
        /*订阅管理*/
        mCompositeDisposable.add(mObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("RxManager", throwable.getMessage());
//                        throwable.printStackTrace();
                    }
                }));
    }

    /**
     * 单纯的Observables 和 Subscribers管理
     *
     * @param disposable
     */
    public void add(Disposable disposable) {
        /*订阅管理*/
        mCompositeDisposable.add(disposable);
    }

    /**
     * 单个presenter生命周期结束，取消订阅和所有rxbus观察
     */
    public void clear() {
        mCompositeDisposable.dispose();// 取消所有订阅
        for (Map.Entry<String, Observable<?>> entry : mObservables.entrySet()) {
            mRxBus.unregister(entry.getKey(), entry.getValue());// 移除rxbus观察
        }
    }

    /**
     * 发送RxBus消息
     *
     * @param tag     标记
     * @param content 消息内容
     */
    public void post(Object tag, Object content) {
        mRxBus.post(tag, content);
    }
}
