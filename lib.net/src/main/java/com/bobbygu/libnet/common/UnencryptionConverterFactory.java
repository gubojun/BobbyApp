package com.bobbygu.libnet.common;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * UnencryptionConverterFactory
 * 不加密转换工厂类，只对参数进行格式转换，不进行加密
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/17 15:49 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class UnencryptionConverterFactory extends Converter.Factory {
    private final Gson gson;

    public static UnencryptionConverterFactory create(Gson gson) {
        return new UnencryptionConverterFactory(gson);
    }

    private UnencryptionConverterFactory(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type,
                                                            Annotation[] annotations,
                                                            Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new EncryptionResponseConverter<>(type, gson, adapter, false);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type,
                                                          Annotation[] parameterAnnotations,
                                                          Annotation[] methodAnnotations,
                                                          Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new EncryptionRequestConverter<>(gson, adapter, false);
    }
}
