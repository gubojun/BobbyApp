/*
 * Copyright (c) 2016 咖枯 <kaku201313@163.com | 3772304@qq.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bobbygu.libnet.common;

public class ApiConstants {
    private static String PRODUCT_HOST = "";
    private static final String PRE_PRODUCT_HOST = "https://153.35.85.252/";
    private static final String TEST_HOST = "http://166.8.62.232:8080/";

    public static void setProductHost(String host) {
        PRODUCT_HOST = host;
    }

    /**
     * 获取对应的host
     *
     * @param hostType host类型
     * @return host
     */
    public static String getHost(int hostType) {
        String host;
        switch (hostType) {
            case HostType.PRODUCT_HOST:
            case HostType.PRODUCT_UNENCRYPTION_HOST:
                host = PRODUCT_HOST;
                break;
            case HostType.TEST_HOST:
                host = TEST_HOST;
                break;
            case HostType.WEB_HOST:
                host = "http://192.168.1.128:8080/";
                break;
            default:
                host = "http://localhost:8080/";
                break;
        }
        return host;
    }
}
