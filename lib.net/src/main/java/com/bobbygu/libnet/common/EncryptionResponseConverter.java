package com.bobbygu.libnet.common;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * EncryptionResponseConverter 对http请求结果进行解密
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/17 15:34 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

final class EncryptionResponseConverter<T> implements Converter<ResponseBody, T> {
    private final Type mType;
    private final Gson gson;
    private final TypeAdapter<T> adapter;
    private final boolean encryption;

    EncryptionResponseConverter(Type type, Gson gson, TypeAdapter<T> adapter, boolean encryption) {
        this.mType = type;
        this.gson = gson;
        this.adapter = adapter;
        this.encryption = encryption;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T convert(ResponseBody value) throws IOException {
        JsonReader jsonReader = gson.newJsonReader(value.charStream());
        try {
            return adapter.read(jsonReader);
        } finally {
            value.close();
        }
    }
}
