package com.bobbygu.libnet.common;

import com.alibaba.fastjson.JSONObject;
import com.bobbygu.libnet.bean.DataResponse;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * ApiService
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/14 18:24 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public interface ApiService {
    @POST("{action}")
    @Headers({"Connection: Keep-Alive", "Accept: application/json"})
    Observable<String> postString(@Path("action") String action,
                                  @Body Map<String, String> requestMap);

    @POST("home/{action}")
    @Headers({"Connection: Keep-Alive", "Accept: application/json"})
    Observable<ResponseBody> postResponseBody(@Path(value = "action") String action,
                                              @Body Map<String, String> requestMap);

    @GET
    @Headers({"Connection: Keep-Alive", "Accept: application/json"})
    Observable<ResponseBody> postDownload(@Url String url);

    @POST("{action}")
    @Headers({"Connection: Keep-Alive", "Accept: application/json"})
    Observable<Response> postResponse(@Path("action") String action,
                                      @Body Map<String, String> requestMap);

    @POST("home/{action}")
    @Headers({"Connection: Keep-Alive", "Accept: application/json"})
    Observable<DataResponse<JSONObject>> postAction(@Path("action") String action,
                                                    @Body Map<String, String> requestMap);

    @POST("home/{action}")
    @Headers({"Connection: Keep-Alive", "Accept: application/json"})
    Observable<DataResponse<JSONObject>> postAction(@Path("action") String action);

    @Multipart
    @POST("home/upload")
    Observable<DataResponse<JSONObject>> upload(@Part("description") RequestBody description,
                                                @PartMap() Map<String, RequestBody> maps);
}
