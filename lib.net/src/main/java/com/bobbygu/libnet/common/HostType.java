/*
 * Copyright (c) 2016 咖枯 <kaku201313@163.com | 3772304@qq.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bobbygu.libnet.common;

public class HostType {

    /**
     * 多少种Host类型
     */
    public static final int TYPE_COUNT = 5;

    /**
     * 生产的host
     */
    public static final int PRODUCT_HOST = 1;
    public static final int PRODUCT_UNENCRYPTION_HOST = 2;

    public static final int DOWNLOAD_HOST = 5;

    /**
     * 测试的host
     */
    public static final int TEST_HOST = 3;

    /**
     * web的host
     */
    public static final int WEB_HOST = 4;

}
