package com.bobbygu.libnet.common;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Converter;

/**
 * EncryptionRequestConverter
 * 对http请求参数进行格式转换
 * <p>
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/17 15:46 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
final class EncryptionRequestConverter<T> implements Converter<T, RequestBody> {
    private final Gson gson;
    private final TypeAdapter<T> adapter;
    private boolean encryption;
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    EncryptionRequestConverter(Gson gson, TypeAdapter<T> adapter, boolean encryption) {
        this.gson = gson;
        this.adapter = adapter;
        this.encryption = encryption;
    }

    @Override
    @SuppressWarnings("unchecked")
    public RequestBody convert(T value) throws IOException {
        Buffer buffer = new Buffer();
        Writer writer = new OutputStreamWriter(buffer.outputStream(), UTF_8);
        JsonWriter jsonWriter = gson.newJsonWriter(writer);
        adapter.write(jsonWriter, value);
        jsonWriter.close();
        return RequestBody.create(MEDIA_TYPE, buffer.readByteString());
    }

}
