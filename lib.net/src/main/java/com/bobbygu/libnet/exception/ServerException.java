package com.bobbygu.libnet.exception;

/**
 * ServerException 服务器请求异常
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/16 8:58 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class ServerException extends Exception {

    public ServerException(String msg) {
        super(msg);
    }

}
