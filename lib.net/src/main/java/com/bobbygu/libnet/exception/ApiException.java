package com.bobbygu.libnet.exception;

/**
 * ApiException
 * <p>
 * author: 顾博君 <br>
 * time:   2017/8/16 9:16 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class ApiException extends Exception {

    private final int code;
    private String displayMessage;

    public static final int UNKNOWN = 1000;
    public static final int PARSE_ERROR = 1001;

    public ApiException(Throwable throwable, int code) {
        super(throwable);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }

    public void setDisplayMessage(String msg) {
        this.displayMessage = msg + "(code:" + code + ")";
    }
}
