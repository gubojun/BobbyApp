## BobbyApp项目简介
应用名称：BobbyApp

![](image/app_icon_1.png)

应用简介：
自己的练习项目，想应用最新技术，并记录一些开发中遇到的问题

demo包:[apk](file/app-product-release.apk)
## 项目技术点
1. 使用databinding实现MVVM架构
2. 分module使工程解耦，有利于大型项目的维护
3. 使用kotlin和java混合开发(kotlin是google官方推荐的开发语言，使用起来确实非常方便，上手也不难，相信未来会成为android开发主流语言)
4. 网络层使用RxJava+Retrofit2+OKHttp(目前主流的网络框架)
5. Replugin 插件化
6. 崩溃信息捕获，错误日志上传
7. [推送(使用Socket模仿jpush功能，后台长连接)](app/src/main/java/com/bobbygu/bobbyapp/function/xmpp/BackService.java)
8. [语音合成(集成讯飞)](app/src/main/java/com/bobbygu/bobbyapp/function/xmpp/BackService.java)
- lottie动画库

> - 待应用技术点
- [x]  使用Altas实现插件化和Bundle化(适用于大型项目)
- [ ]  使用Small实现插件化(适用于中小型项目)
- [ ]  VLayout
- [ ]  多机型适配

## 需要注意的问题
1. databinding和kotlin同时使用的时候，在Fragment中会报找不到BR,通过在app模块的build.gradle中添加以下内容解决
```
apply plugin: 'kotlin-kapt'

dependencies {
    kapt "com.android.databinding:compiler:$gradle_version"
}
```
2. RecyclerView在databinding中数据List可以使用ObservableArrayList，可以轻松实现双向绑定

## 项目资源
项目|地址
---|---
客户端 | [https://gitee.com/gubojun/BobbyApp.git](https://gitee.com/gubojun/BobbyApp.git)
服务端|[https://gitee.com/gubojun/BobbyAppServer.git](https://gitee.com/gubojun/BobbyAppServer.git)

## 项目开发环境
--|环境
---|---
系统|window 10
IDE|Android Studio 3.0
JDK|1.8
SDK|26.0.2
Kotlin|1.2.0
Gradle|4.1


## 联系
QQ:1123425821

[CSDN](http://blog.csdn.net/gubojun123)

[Github](https://github.com/gubojun)