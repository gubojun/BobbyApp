// IMyAidlInterface.aidl
package com.bobbygu.bobbyapp;
import com.bobbygu.bobbyapp.function.aidlSample.Book;//注意引用
// Declare any non-default types here with import statements

interface IMyAidlInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);
    String getColor();
    double getWeight();
    Book getBook();
}
