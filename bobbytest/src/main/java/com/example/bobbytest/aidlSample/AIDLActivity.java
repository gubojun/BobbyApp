package com.example.bobbytest.aidlSample;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.view.View;

import com.bobbygu.bobbyapp.IMyAidlInterface;
import com.example.bobbytest.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.orhanobut.logger.Logger;

/**
 * AIDLActivity
 * <p>
 * author: 顾博君 <br>
 * time:   2017/12/5 19:21 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class AIDLActivity extends Activity {
    //IMyAidlInterface所在文件目录必须与service程序中的文件目录一样
    IMyAidlInterface iService;
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            iService = IMyAidlInterface.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            iService = null;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aidl);
    }

    public void getWeight(View view) {
        if (iService == null) {
            new SweetAlertDialog(AIDLActivity.this)
                    .setTitleText(getString(R.string.dialog_title))
                    .setContentText("iService=" + iService)
                    .show();
            Logger.e("iService=" + iService);
        } else {
            try {
                new SweetAlertDialog(AIDLActivity.this)
                        .setTitleText(getString(R.string.dialog_title))
                        .setContentText("iService.getWeight=" + iService.getWeight())
                        .show();
                Logger.e("iService=" + iService.getWeight());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void getColor(View view) {
        if (iService == null) {
            new SweetAlertDialog(AIDLActivity.this)
                    .setTitleText(getString(R.string.dialog_title))
                    .setContentText("iService=" + iService)
                    .show();
            Logger.e("iService=" + iService);
        } else {
            try {
                new SweetAlertDialog(AIDLActivity.this)
                        .setTitleText(getString(R.string.dialog_title))
                        .setContentText("iService.getColor=" + iService.getColor())
                        .show();
                Logger.e("iService=" + iService.getWeight());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void getBook(View view) {
        if (iService == null) {
            new SweetAlertDialog(AIDLActivity.this)
                    .setTitleText(getString(R.string.dialog_title))
                    .setContentText("iService=" + iService)
                    .show();
            Logger.e("iService=" + iService);
        } else {
            try {
                new SweetAlertDialog(AIDLActivity.this)
                        .setTitleText(getString(R.string.dialog_title))
                        .setContentText("iService.getBook=" + iService.getBook())
                        .show();
                Logger.e("iService=" + iService.getWeight());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        final Intent in = new Intent();
//        in.setClassName(this, "com.bobbygu.bobbyapp.function.aidlSample.AIDLService");
        in.setPackage("com.bobbygu.bobbyapp");
        in.setAction("com.bobby.aidl.BobbyService");
        boolean bindService = bindService(in, conn, Context.BIND_AUTO_CREATE);
        Logger.e("bindService=" + bindService);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (conn != null) {
            unbindService(conn);
        }
    }

    public static void startActivity(Activity activity) {
        activity.startActivity(new Intent(activity, AIDLActivity.class));
    }
}
