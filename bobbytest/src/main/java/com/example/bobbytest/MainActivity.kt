package com.example.bobbytest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.bobbytest.aidlSample.AIDLActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_cpt.setOnClickListener(this)
        btn_aidl.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_cpt -> ContentProviderTestActivity.Companion.startActivity(this)
            R.id.btn_aidl -> AIDLActivity.startActivity(this)
        }
    }
}
