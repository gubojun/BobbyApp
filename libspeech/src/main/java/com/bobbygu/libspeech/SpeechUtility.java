package com.bobbygu.libspeech;

import android.content.Context;

/**
 * SpeechUtility
 * <p>
 * author: 顾博君 <br>
 * time:   2018/1/31 18:53 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public class SpeechUtility {
    public static void createUtility(Context context, String s) {
        com.iflytek.cloud.SpeechUtility.createUtility(context, s);
    }
}
