package com.bobbygu.lib.mvvm.common;

import android.support.annotation.LayoutRes;

/**
 * BaseView
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/19 14:10 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public interface BaseView {
    /*********************子类实现***********************/
    //获取布局文件
    @LayoutRes
    int getLayoutId();

    //获取 BR 参数
    int getBR();

    //页面初始化操作
    void initView();

    //页面的 finish 操作
    void finish();
}
