package com.bobbygu.lib.mvvm.common;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * TUtil  泛型类反射获取对象
 * <p>
 * author: 顾博君 <br>
 * time:   2017/11/9 8:51 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */

public class TUtil {
    public static <T> T getT(Object o, int i) {
        try {
            Type type = o.getClass().getGenericSuperclass();
            if (type instanceof ParameterizedType) {
                return ((Class<T>) ((ParameterizedType) type).getActualTypeArguments()[i]).newInstance();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Class<?> forName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
