
package com.bobbygu.lib.mvvm.common;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import java.util.Stack;

/**
 * AppManager activity管理
 * <p>
 * author: 顾博君 <br>
 * time:   2017/9/4 16:52 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 *
 * @since 1.1.2
 */
public class AppManager {
//    private static Stack<Activity> activityStack;
//    private volatile static AppManager instance;

    private AppManager() {
    }

    /**
     * 单一实例
     */
//    public static AppManager getAppManager() {
//        if (instance == null) {
//            synchronized (AppManager.class) {
//                if (instance == null) {
//                    instance = new AppManager();
//                    instance.activityStack = new Stack();
//                }
//            }
//
//        }
//        return instance;
//    }
    public static AppManager getAppManager() {
        return AppManagerHolder.instance;
    }

    private static Stack<Activity> getActivityStack() {
        return AppManagerHolder.activityStack;
    }

    private static class AppManagerHolder {
        private static final AppManager instance = new AppManager();
        private static final Stack<Activity> activityStack = new Stack<>();
    }

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(Activity activity) {
        getActivityStack().add(activity);
    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        try {
            return getActivityStack().lastElement();
        } catch (Exception e) {
//            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取当前Activity的前一个Activity
     */
    public Activity preActivity() {
        int index = getActivityStack().size() - 2;
        if (index < 0) {
            return null;
        }
        return getActivityStack().get(index);
    }

    /**
     * 结束当前Activity（堆栈中最后一个压入的）
     */
    public void finishActivity() {
        Activity activity = getActivityStack().lastElement();
        finishActivity(activity);
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            getActivityStack().remove(activity);
            activity.finish();
        }
    }

    /**
     * 移除指定的Activity
     */
    public void removeActivity(Activity activity) {
        if (activity != null) {
            getActivityStack().remove(activity);
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        try {
            for (Activity activity : getActivityStack()) {
                if (activity.getClass().equals(cls)) {
                    finishActivity(activity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = getActivityStack().size(); i < size; i++) {
            if (null != getActivityStack().get(i)) {
                getActivityStack().get(i).finish();
            }
        }
        getActivityStack().clear();
    }

    /**
     * 返回到指定的activity
     *
     * @param cls
     */
    public void returnToActivity(Class<?> cls) {
        while (getActivityStack().size() != 0)
            if (getActivityStack().peek().getClass() == cls) {
                break;
            } else {
                finishActivity(getActivityStack().peek());
            }
    }


    /**
     * 是否已经打开指定的activity
     *
     * @param cls
     * @return
     */
    public boolean isOpenActivity(Class<?> cls) {
        if (getActivityStack() != null) {
            for (int i = 0, size = getActivityStack().size(); i < size; i++) {
                if (cls == getActivityStack().peek().getClass()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 退出应用程序
     *
     * @param context      上下文
     * @param isBackground 是否开开启后台运行
     */
    void AppExit(Context context, Boolean isBackground) {
        try {
            finishAllActivity();
            ActivityManager activityMgr = (ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE);
            activityMgr.killBackgroundProcesses(context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 注意，如果您有后台程序运行，请不要支持此句子
            if (!isBackground) {
                System.exit(0);
            }
        }
    }
}