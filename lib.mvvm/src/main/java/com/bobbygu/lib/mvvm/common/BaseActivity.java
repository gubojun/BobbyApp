package com.bobbygu.lib.mvvm.common;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bobbygu.lib.mvvm.R;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;


/**
 * BaseActivity MVVM 基类 Activity
 * author:张冠之 and 顾博君
 * time: 2017/9/2 下午11:05
 * e-mail: guanzhi.zhang@sojex.cn
 */

public abstract class BaseActivity<VM extends BaseViewModel, M extends BaseModel> extends AppCompatActivity implements BaseView {
    private FragmentTransaction mFragmentTransaction;
    public String TAG;
    protected VM mViewModel;
    protected ViewDataBinding mViewDataBinding;
    protected Context mContext;
    private boolean canBack = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mViewDataBinding == null) {
            mContext = this;
            mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());

            //反射生成泛型类对象
            mViewModel = TUtil.getT(this, 0);
            M model = TUtil.getT(this, 1);

            //VM 和 View 绑定
            if (mViewModel != null) {
                mViewModel.setContext(mContext);
                mViewModel.setModel(model);
                mViewModel.attachView(this);
            }

            //Model 和 VM 绑定
            if (model != null)
                model.attachViewModel(mViewModel);

            //DataBinding 绑定
            if (getBR() != 0)
                mViewDataBinding.setVariable(getBR(), mViewModel);
        } else {
            setContentView(getLayoutId());
        }
        initView();
        if (mViewModel != null)
            mViewModel.init();
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        TAG = getComponentName().getShortClassName();
        if (canBack) {
            SlidrConfig config = new SlidrConfig.Builder()
//                  .position(SlidrPosition.LEFT|RIGHT|TOP|BOTTOM|VERTICAL|HORIZONTAL)
                    .sensitivity(1f)
//                 .scrimColor(Color.BLACK)
                    .scrimStartAlpha(0.8f)
                    .scrimEndAlpha(0f)
                    .velocityThreshold(2400)
                    .distanceThreshold(0.25f)
                    .edge(true | false)
                    .edgeSize(0.18f) // The % of the screen that counts as the edge, default 18%
//                 .listener(new SlidrListener(){...})
                    .build();

            Slidr.attach(this, config);
        }
    }

    public void setSlideBack(boolean canBack) {
        this.canBack = canBack;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mViewModel != null)
            mViewModel.detachView();
    }

    public VM getViewModel() {
        return mViewModel;
    }

    public ViewDataBinding getViewDataBinding() {
        return mViewDataBinding;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }


    @Override
    public int getBR() {
        return 0;
    }

    public void startActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    public void finishActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.slide_in_anim, R.anim.push_right_out);
        }
    }

    public void startFragment(Fragment fragment, @IdRes int layout) {
        mFragmentTransaction.replace(layout, fragment).commit();
    }
}
