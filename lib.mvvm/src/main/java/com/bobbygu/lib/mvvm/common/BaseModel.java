package com.bobbygu.lib.mvvm.common;

/**
 * BaseModel 子类必须是public类型
 * <p>
 * author: 顾博君 <br>
 * time:   2017/10/19 13:46 <br>
 * e-mail: gubojun@csii.com.cn <br>
 * </p>
 */
public abstract class BaseModel<VM extends BaseViewModel> {
    private VM mViewModel;

    public VM getViewModel() {
        return mViewModel;
    }

    public void attachViewModel(VM viewModel) {
        mViewModel = viewModel;
    }
}
